package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.languageMap.CompoundLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/compoundLanguageMaps")
@Slf4j
public class CompoundLanguageMapController extends GenericLanguageMapController<JsonNode> {

    private final CompoundLanguageMapService compoundLanguageMapService;

    public CompoundLanguageMapController(final CompoundLanguageMapService compoundLanguageMapService) {
        this.compoundLanguageMapService = compoundLanguageMapService;
        this.init(compoundLanguageMapService);
    }

}
