package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.languageMap.FixingLocationLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fixingLocationLanguageMaps")
@Slf4j
public class FixingLocationLanguageMapController extends GenericLanguageMapController<JsonNode> {
    private final FixingLocationLanguageMapService fixingLocationLanguageMapService;

    public FixingLocationLanguageMapController(final FixingLocationLanguageMapService fixingLocationLanguageMapService) {
        this.fixingLocationLanguageMapService = fixingLocationLanguageMapService;
        this.init(fixingLocationLanguageMapService);
    }

}
