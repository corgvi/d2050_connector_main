package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.MarkIdentService;
import consulting.sit.catenax.service.glossary.languageMap.MarkIdentLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/markIdentLanguageMaps")
@Slf4j
public class MarkIdentLanguageMapController extends GenericLanguageMapController<JsonNode> {
    private final MarkIdentLanguageMapService markIdentLanguageMap;

    public MarkIdentLanguageMapController(final MarkIdentLanguageMapService markIdentLanguageMap) {
        this.markIdentLanguageMap = markIdentLanguageMap;
        this.init(markIdentLanguageMap);
    }

}

