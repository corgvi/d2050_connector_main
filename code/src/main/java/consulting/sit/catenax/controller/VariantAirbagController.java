package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.VariantAirbagService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/variantAirbag")
@Slf4j
public class VariantAirbagController {
    @Autowired
    private VariantAirbagService variantAirbagService;


    @GetMapping("/{id}/{language}/details")
    public ResponseEntity<JsonNode> getVariantAirbagDetails(@PathVariable int id, @PathVariable String language) {
        JsonNode jsonNode = variantAirbagService.getVariantAirbagDetails(id, language);
        if (jsonNode == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(jsonNode);
    }

    @PutMapping("/{id}")
    public ResponseEntity<JsonNode> updateVariantAirbag(@RequestBody JsonNode updateVariantAirbag, @PathVariable String id) {
        try {
            return ResponseEntity.ok(variantAirbagService.updateVariantAirbag(updateVariantAirbag, id));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<JsonNode> updateFieldOfVariantAirbag(@RequestBody JsonNode updateVariantAirbag, @PathVariable String id) {
        try {
            return ResponseEntity.ok(variantAirbagService.updateVariantAirbagByField(updateVariantAirbag, id));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping()
    public ResponseEntity<JsonNode> newVariantAirbag(@RequestBody JsonNode newVariantAirbag) {
        try {
            return ResponseEntity.ok(variantAirbagService.newVariantAirbag(newVariantAirbag));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<JsonNode> deleteVariantAirbag(@PathVariable String id) {
        try {
            variantAirbagService.deleteVariantAirbag(id);
            return ResponseEntity.ok().build();
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.noContent().build();
        }

    }
}
