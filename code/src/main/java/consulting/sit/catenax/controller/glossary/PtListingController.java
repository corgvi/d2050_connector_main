package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.PtListingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ptListings")
@Slf4j
public class PtListingController extends GenericController<JsonNode> {
    private final PtListingService ptListingService;

    public PtListingController(PtListingService ptListingService) {
        this.ptListingService = ptListingService;
        this.init(ptListingService);
    }
}

