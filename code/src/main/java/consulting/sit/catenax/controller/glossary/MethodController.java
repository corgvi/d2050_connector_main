package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.MethodService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/methods")
@Slf4j
public class MethodController extends GenericController<JsonNode> {
    private final MethodService methodService;

    public MethodController(MethodService methodService) {
        this.methodService = methodService;
        this.init(methodService);
    }
}
