package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.ComponentReferenceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/componentReferences")
@Slf4j
public class ComponentReferenceController extends GenericProjectController<JsonNode>{

    private final ComponentReferenceService componentReferenceService;

    public ComponentReferenceController(final ComponentReferenceService componentReferenceService) {
        this.componentReferenceService = componentReferenceService;
        this.init(componentReferenceService);
    }

}
