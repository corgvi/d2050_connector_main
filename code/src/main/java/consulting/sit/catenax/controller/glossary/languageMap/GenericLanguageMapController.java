package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.glossary.GenericService;
import consulting.sit.catenax.service.glossary.languageMap.GenericLanguageMapService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.HttpClientErrorException;

@Component
public abstract class GenericLanguageMapController<T> {
    private GenericLanguageMapService<T> service;
    public void init(GenericLanguageMapService service) {
        this.service = service;
    }

    @GetMapping("/{language}")
    public ResponseEntity<T> getAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(service.getAllByLanguage(language));
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<T> findById(@PathVariable String id, @PathVariable String language) {
        try {
            return ResponseEntity.ok(service.findById(id, language));
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.badRequest().build();
        }

    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<T> update (@RequestBody JsonNode node, @PathVariable String id, @PathVariable String language ) {
        try {
            return ResponseEntity.ok(service.update(node, id, language));
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).build();
        }
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<T> delete(@PathVariable String id, @PathVariable String language) {
        try {
            service.deleteLanguage(id, language);
            return ResponseEntity.ok().build();
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.badRequest().build();
        }

    }
}
