package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.FixingService;
import consulting.sit.catenax.service.GenericProjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/fixings")
@Slf4j
public class FixingController extends GenericProjectController<JsonNode> {

    private final FixingService fixingService;

    public FixingController(final FixingService fixingService) {
        this.fixingService = fixingService;
        this.init(fixingService);
    }

    @PatchMapping("/update")
    public ResponseEntity<JsonNode> updateFieldOfFixings(@RequestBody JsonNode updateFixing) {
        try {
            return ResponseEntity.ok(fixingService.updateFixings(updateFixing));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }
}
