package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.AirbagPositionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/airbagPositions")
@Slf4j
public class AirbagPositionController extends GenericController<JsonNode>{

    private final AirbagPositionService airbagPositionService;

    public AirbagPositionController(final AirbagPositionService airbagPositionService) {
        this.airbagPositionService = airbagPositionService;
        this.init(airbagPositionService);
    }

}
