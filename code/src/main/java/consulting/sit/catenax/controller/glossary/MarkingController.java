package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.MarkingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/markings")
@Slf4j
public class MarkingController extends GenericController<JsonNode> {
    private final MarkingService markingService;

    public MarkingController(final MarkingService markingService) {
        this.markingService = markingService;
        this.init(markingService);
    }
}
