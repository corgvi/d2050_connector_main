package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.RgIsoGroupValueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rgIsoGroupValues")
@Slf4j
public class RgIsoGroupValueController extends GenericController<JsonNode> {
    private final RgIsoGroupValueService rgIsoGroupValueService;

    public RgIsoGroupValueController(RgIsoGroupValueService rgIsoGroupValueService) {
        this.rgIsoGroupValueService = rgIsoGroupValueService;
        this.init(rgIsoGroupValueService);
    }
}
