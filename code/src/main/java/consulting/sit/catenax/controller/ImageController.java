package consulting.sit.catenax.controller;

import consulting.sit.catenax.service.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Image controller for managing upload and download image
 */
@RestController
@RequestMapping("/images")
@Slf4j
public class ImageController {
    @Value("${d2050.service.baseurl}")
    private String baseUrl;

    @Autowired
    private ImageService imageService;

    @PostMapping("/upload")
    public ResponseEntity<String> uploadImage(@RequestParam("file") MultipartFile file) {
        String uploadedImage = this.imageService.uploadImage(file);
        if (StringUtils.isNotEmpty(uploadedImage)) {
            return ResponseEntity.ok().body(uploadedImage);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Get image from path
     * @param relativeUrl
     * @return
     */
    @GetMapping("/download/{relativeUrl}")
    public ResponseEntity<Resource> downloadImage(@PathVariable String relativeUrl){
        // Logic to retrieve the image file based on relative URL
        // For example, if images are stored in a local directory:
        final Resource resource;
        try {
            resource = this.imageService.downloadImage(relativeUrl);
            String fileName = resource.getFilename();
            String imageExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
            // Set the Content-Type and Content-Disposition headers based on file format
            HttpHeaders headers = new HttpHeaders();
            if ("jpg".equalsIgnoreCase(imageExtension) || "jpeg".equalsIgnoreCase(imageExtension)) {
                headers.add(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE);
            } else if ("png".equalsIgnoreCase(imageExtension)) {
                headers.add(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE);
            }
            // Return the image file as a response with appropriate headers
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(resource.contentLength())
                    .body(resource);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }
}
