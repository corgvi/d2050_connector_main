package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.MaterialGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/materialGroups")
@Slf4j
public class MaterialGroupController extends GenericController<JsonNode> {
    private final MaterialGroupService materialGroupService;

    public MaterialGroupController(final MaterialGroupService materialGroupService) {
        this.materialGroupService = materialGroupService;
        this.init(materialGroupService);
    }
}
