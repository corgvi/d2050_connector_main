package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.FractionWasteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fractionWastes")
@Slf4j
public class FractionWasteController extends GenericController<JsonNode> {

    private final FractionWasteService fractionWasteService;

    public FractionWasteController(final FractionWasteService fractionWasteService) {
        this.fractionWasteService = fractionWasteService;
        this.init(fractionWasteService);
    }
}
