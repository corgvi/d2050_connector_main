package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.ActivityAreaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/activityAreas")
@Slf4j
public class ActivityAreaController extends GenericController<JsonNode>{

    private final ActivityAreaService activityAreaService;

    public ActivityAreaController(final ActivityAreaService activityAreaService) {
        this.activityAreaService = activityAreaService;
        this.init(activityAreaService);
    }

}
