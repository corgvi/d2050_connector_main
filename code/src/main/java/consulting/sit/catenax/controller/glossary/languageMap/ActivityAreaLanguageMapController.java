package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.languageMap.ActivityAreaLanguageMapService;
import consulting.sit.catenax.service.glossary.languageMap.GenericLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/activityAreaLanguageMaps")
@Slf4j
public class ActivityAreaLanguageMapController extends GenericLanguageMapController<JsonNode> {

    private final ActivityAreaLanguageMapService activityAreaLanguageMapService;

    public ActivityAreaLanguageMapController(final ActivityAreaLanguageMapService activityAreaLanguageMapService) {
        this.activityAreaLanguageMapService = activityAreaLanguageMapService;
        this.init(activityAreaLanguageMapService);
    }

}
