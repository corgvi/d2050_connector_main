package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.ToolTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/toolTypes")
@Slf4j
public class ToolTypeController extends GenericController<JsonNode> {
    private final ToolTypeService toolTypeService;

    public ToolTypeController(ToolTypeService toolTypeService) {
        this.toolTypeService = toolTypeService;
        this.init(toolTypeService);
    }
}
