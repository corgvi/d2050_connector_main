package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.RgIsoGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rgIsoGroups")
@Slf4j
public class RgIsoGroupController extends GenericController<JsonNode> {
    private final RgIsoGroupService rgIsoGroupService;

    public RgIsoGroupController(RgIsoGroupService rgIsoGroupService) {
        this.rgIsoGroupService = rgIsoGroupService;
        this.init(rgIsoGroupService);
    }
}
