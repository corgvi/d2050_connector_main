package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.UiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/uis")
@Slf4j
public class UiController extends GenericController<JsonNode> {
    private final UiService uiService;

    public UiController(UiService uiService) {
        this.uiService = uiService;
        this.init(uiService);
    }
}
