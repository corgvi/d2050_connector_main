package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.PartService;
import consulting.sit.catenax.service.glossary.ToolMainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/toolMains")
@Slf4j
public class ToolMainController extends GenericController<JsonNode>{
    private final ToolMainService toolMainService;

    public ToolMainController(final ToolMainService toolMainService) {
        this.toolMainService = toolMainService;
        this.init(toolMainService);
    }

}
