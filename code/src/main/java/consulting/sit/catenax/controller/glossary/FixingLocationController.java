package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.FixingLocationService;
import consulting.sit.catenax.service.glossary.FixingMainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fixingLocations")
@Slf4j
public class FixingLocationController extends GenericController<JsonNode>{
    private final FixingLocationService fixingLocationService;

    public FixingLocationController(final FixingLocationService fixingLocationService) {
        this.fixingLocationService = fixingLocationService;
        this.init(fixingLocationService);
    }

}
