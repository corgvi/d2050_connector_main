package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.RgStrategyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rgStrategies")
@Slf4j
public class RgStrategyController extends GenericController<JsonNode> {
    private final RgStrategyService rgStrategyService;

    public RgStrategyController(RgStrategyService rgStrategyService) {
        this.rgStrategyService = rgStrategyService;
        this.init(rgStrategyService);
    }
}
