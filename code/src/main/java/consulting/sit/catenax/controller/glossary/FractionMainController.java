package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.FractionMainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fractionMains")
@Slf4j
public class FractionMainController extends GenericController<JsonNode>{

    private final FractionMainService fractionMainService;

    public FractionMainController(final FractionMainService fractionMainService) {
        this.fractionMainService = fractionMainService;
        this.init(fractionMainService);
    }

}
