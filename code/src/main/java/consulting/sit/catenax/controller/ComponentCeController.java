package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.ComponentCeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/componentCes")
@Slf4j
public class ComponentCeController extends GenericProjectController<JsonNode>{

    private final ComponentCeService componentCeService;

    public ComponentCeController(final ComponentCeService componentCeService) {
        this.componentCeService = componentCeService;
        this.init(this.componentCeService);
    }

    @GetMapping("/componentMain/{componentId}")
    public ResponseEntity<JsonNode> findByComponentId(@PathVariable int componentId) {
        try {
            return ResponseEntity.ok(componentCeService.findByComponentId(componentId));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

}
