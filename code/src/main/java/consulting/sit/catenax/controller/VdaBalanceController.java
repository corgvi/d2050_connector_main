package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.VdaBalanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/vdaBalances")
@Slf4j
public class VdaBalanceController extends GenericProjectController<JsonNode>{

    private final VdaBalanceService vdaBalanceService;

    public VdaBalanceController(final VdaBalanceService vdaBalanceService) {
        this.vdaBalanceService = vdaBalanceService;
        this.init(vdaBalanceService);
    }

    @GetMapping("/componentMain/{componentId}/materialGroup/{materialGroupId}")
    public ResponseEntity<JsonNode> findByComponentIdAndMaterialGroupId(@PathVariable int componentId, @PathVariable int materialGroupId) {
        try {
            return ResponseEntity.ok(vdaBalanceService.findByComponentIdAndMaterialGroupId(componentId, materialGroupId));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

}
