package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.MaterialMainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/materialMains")
@Slf4j
public class MaterialMainController extends GenericController<JsonNode>{

    private final MaterialMainService materialMainService;

    public MaterialMainController(final MaterialMainService materialMainService) {
        this.materialMainService = materialMainService;
        this.init(materialMainService);
    }

}
