package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.RgMaterialGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rgMaterialGroups")
@Slf4j
public class RgMaterialGroupController extends GenericController<JsonNode> {
    private final RgMaterialGroupService materialGroupService;

    public RgMaterialGroupController(RgMaterialGroupService materialGroupService) {
        this.materialGroupService = materialGroupService;
        this.init(materialGroupService);
    }
}
