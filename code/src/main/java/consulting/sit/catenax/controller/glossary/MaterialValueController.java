package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.MaterialValueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/materialValues")
@Slf4j
public class MaterialValueController extends GenericController<JsonNode> {
    private final MaterialValueService materialValueService;

    public MaterialValueController(final MaterialValueService materialValueService) {
        this.materialValueService = materialValueService;
        this.init(materialValueService);
    }
}

