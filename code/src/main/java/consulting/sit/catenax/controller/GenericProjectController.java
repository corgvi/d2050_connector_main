package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.GenericProjectService;
import consulting.sit.catenax.service.glossary.GenericService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.HttpClientErrorException;

@Component
public abstract class GenericProjectController<T>  {

    private GenericProjectService<T> service;
    public void init(GenericProjectService service) {
        this.service = service;
    }

    @PostMapping()
    public ResponseEntity<T> create (@RequestBody T newNode) {
        try {
            return ResponseEntity.ok(service.create(newNode));
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<T> delete(@PathVariable String id) {
        try {
            service.delete(id);
            return ResponseEntity.ok().build();
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.badRequest().build();
        }

    }

    @PatchMapping("/{id}")
    public ResponseEntity<T> updateField(@RequestBody JsonNode updateObject, @PathVariable String id) {
        try {
            return ResponseEntity.ok(service.updateByField(updateObject, id));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.status(ex.getStatusCode()).build();
        }
    }
}
