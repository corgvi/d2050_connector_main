package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.VariantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/variants")
@Slf4j
public class VariantController {
    @Autowired
    private VariantService variantService;

    @GetMapping("/{id}/parts/{language}/details")
    public ResponseEntity<JsonNode> getPartDetails(@PathVariable int id, @PathVariable String language) {
        JsonNode jsonNode = variantService.getPartDetails(id, language);
        if (jsonNode == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(jsonNode);
    }

    @GetMapping("/{id}/{language}/details")
    public ResponseEntity<JsonNode> getVariantDetails(@PathVariable int id, @PathVariable String language) {
        JsonNode jsonNode = variantService.getVariantDetails(id, language);
        if (jsonNode == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(jsonNode);
    }

    @PutMapping("/{id}")
    public ResponseEntity<JsonNode> updateVariant(@RequestBody JsonNode updateVariant, @PathVariable String id) {
        return ResponseEntity.ok(variantService.updateVariant(updateVariant, id));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<JsonNode> updateFieldOfVariant(@RequestBody JsonNode updateVariant, @PathVariable String id) {
        return ResponseEntity.ok(variantService.updateVariantByField(updateVariant, id));
    }

    @PostMapping("/variant")
    public ResponseEntity<JsonNode> newVariant(@RequestBody JsonNode newVariant) {
        try {
            return ResponseEntity.ok(variantService.newVariant(newVariant));
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).build();
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<JsonNode> deleteVariant(@PathVariable String id) {
        try {
            variantService.deleteVariant(id);
            return ResponseEntity.ok().build();
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.noContent().build();
        }

    }
}
