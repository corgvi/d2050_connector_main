package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.ComponentMarkingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/componentMarkings")
@Slf4j
public class ComponentMarkingController extends GenericProjectController<JsonNode>{

    private final ComponentMarkingService componentMarkingService;

    public ComponentMarkingController(final ComponentMarkingService componentMarkingService) {
        this.componentMarkingService = componentMarkingService;
        this.init(componentMarkingService);
    }

}
