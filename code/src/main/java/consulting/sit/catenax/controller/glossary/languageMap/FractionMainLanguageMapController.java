package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.languageMap.FractionMainLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fractionMainLanguageMaps")
@Slf4j
public class FractionMainLanguageMapController extends GenericLanguageMapController<JsonNode> {

    private final FractionMainLanguageMapService fractionMainLanguageMapService;

    public FractionMainLanguageMapController(final FractionMainLanguageMapService fractionMainLanguageMapService) {
        this.fractionMainLanguageMapService = fractionMainLanguageMapService;
        this.init(fractionMainLanguageMapService);
    }

}
