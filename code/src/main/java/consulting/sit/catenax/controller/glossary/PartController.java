package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.PartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/parts")
@Slf4j
public class PartController extends GenericController<JsonNode>{
    private final PartService partService;

    public PartController(final PartService partService) {
        this.partService = partService;
        this.init(partService);
    }

}
