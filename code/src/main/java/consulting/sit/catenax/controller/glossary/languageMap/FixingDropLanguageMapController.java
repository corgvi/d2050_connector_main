package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.FixingDropService;
import consulting.sit.catenax.service.glossary.languageMap.FixingDropLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fixingDropLanguageMaps")
@Slf4j
public class FixingDropLanguageMapController extends GenericLanguageMapController<JsonNode> {
    private final FixingDropLanguageMapService fixingDropLanguageMapService;

    public FixingDropLanguageMapController(final FixingDropLanguageMapService fixingDropLanguageMapService) {
        this.fixingDropLanguageMapService = fixingDropLanguageMapService;
        this.init(fixingDropLanguageMapService);
    }
}
