package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.PtMaterialMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ptMaterialMaps")
@Slf4j
public class PtMaterialMapController extends GenericController<JsonNode> {
    private final PtMaterialMapService ptMaterialMapService;

    public PtMaterialMapController(PtMaterialMapService ptMaterialMapService) {
        this.ptMaterialMapService = ptMaterialMapService;
        this.init(ptMaterialMapService);
    }
}
