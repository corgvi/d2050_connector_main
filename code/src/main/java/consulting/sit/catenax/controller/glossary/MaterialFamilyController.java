package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.MaterialFamilyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/materialFamilies")
@Slf4j
public class MaterialFamilyController extends GenericController<JsonNode> {
    private final MaterialFamilyService materialFamilyService;

    public MaterialFamilyController(final MaterialFamilyService materialFamilyService) {
        this.materialFamilyService = materialFamilyService;
        this.init(materialFamilyService);
    }
}
