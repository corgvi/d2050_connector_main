package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.ComponentDescriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/componentDescriptions")
@Slf4j
public class ComponentDescriptionController extends GenericProjectController<JsonNode>{

    private final ComponentDescriptionService componentDescriptionService;

    public ComponentDescriptionController(final ComponentDescriptionService componentDescriptionService) {
        this.componentDescriptionService = componentDescriptionService;
        this.init(componentDescriptionService);
    }

}
