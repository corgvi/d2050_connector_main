package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.MaterialFamilyValueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/materialFamilyValues")
@Slf4j
public class MaterialFamilyValueController extends GenericController<JsonNode> {
    private final MaterialFamilyValueService materialFamilyValueService;

    public MaterialFamilyValueController(final MaterialFamilyValueService materialFamilyValueService) {
        this.materialFamilyValueService = materialFamilyValueService;
        this.init(materialFamilyValueService);
    }
}
