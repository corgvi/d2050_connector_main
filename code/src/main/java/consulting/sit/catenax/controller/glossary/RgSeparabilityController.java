package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.RgSeparabilityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rgSeparability")
@Slf4j
public class RgSeparabilityController extends GenericController<JsonNode> {
    private final RgSeparabilityService rgSeparabilityService;

    public RgSeparabilityController(RgSeparabilityService rgSeparabilityService) {
        this.rgSeparabilityService = rgSeparabilityService;
        this.init(rgSeparabilityService);
    }
}
