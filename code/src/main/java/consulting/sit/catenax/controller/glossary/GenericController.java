package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.glossary.GenericService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.HttpClientErrorException;

@Component
public abstract class GenericController<T> {
    private GenericService<T> service;
    public void init(GenericService service) {
        this.service = service;
    }

    @GetMapping()
    public ResponseEntity<T> getAll() {
        try {
            return ResponseEntity.ok(service.getAll());
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping()
    public ResponseEntity<T> create (@RequestBody T newNode) {
        try {
            return ResponseEntity.ok(service.create(newNode));
        } catch (HttpClientErrorException e) {
            return ResponseEntity.status(e.getStatusCode()).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<T> delete(@PathVariable String id) {
        try {
            service.delete(id);
            return ResponseEntity.ok().build();
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<T> findById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(service.findById(id));
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.badRequest().build();
        }
    }
}
