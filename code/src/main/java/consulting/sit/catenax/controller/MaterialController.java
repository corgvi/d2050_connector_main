package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.MaterialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/materials")
@Slf4j
public class MaterialController {
    @Autowired
    private MaterialService materialService;

    @GetMapping("/{materialId}/{language}/details")
    public ResponseEntity<JsonNode> getComponentDetail(@PathVariable int materialId, @PathVariable String language) {
        return ResponseEntity.ok(materialService.getMaterialDetail(materialId, language));
    }
}
