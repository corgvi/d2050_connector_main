package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.FixingCharService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fixingChars")
@Slf4j
public class FixingCharController extends GenericController<JsonNode> {

    private final FixingCharService fixingCharService;

    public FixingCharController(final FixingCharService fixingCharService) {
        this.fixingCharService = fixingCharService;
        this.init(fixingCharService);
    }
}
