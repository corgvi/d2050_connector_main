package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.FixingMainService;
import consulting.sit.catenax.service.glossary.ToolMainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fixingMains")
@Slf4j
public class FixingMainController extends GenericController<JsonNode>{
    private final FixingMainService fixingMainService;

    public FixingMainController(final FixingMainService fixingMainService) {
        this.fixingMainService = fixingMainService;
        this.init(fixingMainService);
    }

}
