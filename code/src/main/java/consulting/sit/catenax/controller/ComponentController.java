package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.ComponentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/components")
@Slf4j
public class ComponentController extends GenericProjectController<JsonNode>{

    private final ComponentService componentService;

    public ComponentController(final ComponentService componentService) {
        this.componentService = componentService;
        this.init(componentService);
    }

    @GetMapping("/variant/{variantId}/{language}")
    public ResponseEntity<JsonNode> getAllComponentByVariantId(@PathVariable Integer variantId, @PathVariable String language) {
        return ResponseEntity.ok(componentService.getAllComponentByVariant(variantId, language));
    }

    @GetMapping("/tree/{variantId}/{language}")
    public ResponseEntity<JsonNode> getTree(@PathVariable Integer variantId, @PathVariable String language) {
        return ResponseEntity.ok(componentService.getTree(variantId, language));
    }

    @GetMapping("/tree/{variantId}/{language}/first")
    public ResponseEntity<JsonNode> getTreeFirst(@PathVariable Integer variantId, @PathVariable String language) {
        return ResponseEntity.ok(componentService.getFirstChildOfTree(variantId, language));
    }

    @GetMapping("/tree/{componentId}/{language}/level/{level}")
    public ResponseEntity<JsonNode> getComponentTreeByComponentIdAndLevelOfTree(@PathVariable Integer componentId, @PathVariable String language, @PathVariable Integer level) {
        return ResponseEntity.ok(componentService.getChildrenByComponentIdAndLevelOfTree(componentId, language, level));
    }

    @GetMapping("/tree/component/{componentId}/{language}")
    public ResponseEntity<JsonNode> getComponentTreeByComponentId(@PathVariable Integer componentId, @PathVariable String language) {
        return ResponseEntity.ok(componentService.getChildren(componentId, language));
    }

    @GetMapping("/children/{componentId}/{language}")
    public ResponseEntity<JsonNode> getChildrenOfComponentById(@PathVariable Integer componentId, @PathVariable String language) {
        return ResponseEntity.ok(componentService.getChildrenByComponentIdAndLevelOfTree(componentId, language, 1));
    }

    @GetMapping("/{componentId}/{language}/details")
    public ResponseEntity<JsonNode> getComponentDetail(@PathVariable int componentId, @PathVariable String language) {
        return ResponseEntity.ok(componentService.getComponentDetail(componentId, language));
    }

    @GetMapping("/assemblyPart/{language}")
    public ResponseEntity<JsonNode> getAllAssemblyPart(@PathVariable String language) {
        return ResponseEntity.ok(componentService.getAllAssemblyPart(language));
    }

    @GetMapping("/componentDepend/{componentId}")
    public ResponseEntity<JsonNode> getAllComponentDependByComponentId(@PathVariable int componentId) {
        return ResponseEntity.ok(componentService.getAllComponentDependByComponentId(componentId));
    }
}
