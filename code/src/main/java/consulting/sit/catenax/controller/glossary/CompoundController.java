package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.CompoundService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/compounds")
@Slf4j
public class CompoundController extends GenericController<JsonNode>{

    private final CompoundService compoundService;

    public CompoundController(final CompoundService compoundService) {
        this.compoundService = compoundService;
        this.init(compoundService);
    }

}
