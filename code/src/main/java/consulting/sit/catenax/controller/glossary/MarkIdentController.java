package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.MarkIdentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/markIdents")
@Slf4j
public class MarkIdentController extends GenericController<JsonNode>{
    private final MarkIdentService markIdentService;
    public MarkIdentController(final MarkIdentService markIdentService) {
        this.markIdentService = markIdentService;
        this.init(markIdentService);
    }

}

