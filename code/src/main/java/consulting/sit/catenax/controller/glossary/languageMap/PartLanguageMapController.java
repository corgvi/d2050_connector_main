package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.PartService;
import consulting.sit.catenax.service.glossary.languageMap.PartLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/partLanguageMaps")
@Slf4j
public class PartLanguageMapController extends GenericLanguageMapController<JsonNode> {
    private final PartLanguageMapService partLanguageMapService;

    public PartLanguageMapController(final PartLanguageMapService partLanguageMapService) {
        this.partLanguageMapService = partLanguageMapService;
        this.init(partLanguageMapService);
    }

}
