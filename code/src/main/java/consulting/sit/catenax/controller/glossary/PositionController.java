package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.PositionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/positions")
@Slf4j
public class PositionController extends GenericController<JsonNode> {
    private final PositionService positionService;

    public PositionController(PositionService positionService) {
        this.positionService = positionService;
        this.init(positionService);
    }
}
