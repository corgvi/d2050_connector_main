package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.PtTreatmentMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ptTreatmentMaps")
@Slf4j
public class PtTreatmentMapController extends GenericController<JsonNode> {
    private final PtTreatmentMapService ptTreatmentMapService;

    public PtTreatmentMapController(PtTreatmentMapService ptTreatmentMapService) {
        this.ptTreatmentMapService = ptTreatmentMapService;
        this.init(ptTreatmentMapService);
    }
}
