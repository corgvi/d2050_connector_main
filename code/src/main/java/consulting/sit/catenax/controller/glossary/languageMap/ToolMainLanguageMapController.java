package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.ToolMainService;
import consulting.sit.catenax.service.glossary.languageMap.ToolMainLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/toolMainLanguageMaps")
@Slf4j
public class ToolMainLanguageMapController extends GenericLanguageMapController<JsonNode> {
    private final ToolMainLanguageMapService toolMainLanguageMapService;

    public ToolMainLanguageMapController(final ToolMainLanguageMapService toolMainLanguageMapService) {
        this.toolMainLanguageMapService = toolMainLanguageMapService;
        this.init(toolMainLanguageMapService);
    }

}
