package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.RgIsoCalcService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rgIsoCalc")
@Slf4j
public class RgIsoCalcController extends GenericController<JsonNode> {
    private final RgIsoCalcService rgIsoCalcService;

    public RgIsoCalcController(RgIsoCalcService rgIsoCalcService) {
        this.rgIsoCalcService = rgIsoCalcService;
        this.init(rgIsoCalcService);
    }
}
