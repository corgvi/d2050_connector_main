package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.MaterialMainService;
import consulting.sit.catenax.service.glossary.languageMap.MaterialMainLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/materialMainLanguageMaps")
@Slf4j
public class MaterialMainLanguageMapController extends GenericLanguageMapController<JsonNode> {

    private final MaterialMainLanguageMapService materialMainLanguageMapService;

    public MaterialMainLanguageMapController(final MaterialMainLanguageMapService materialMainLanguageMapService) {
        this.materialMainLanguageMapService = materialMainLanguageMapService;
        this.init(materialMainLanguageMapService);
    }

}
