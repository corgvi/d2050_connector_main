package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.ComponentDependService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/componentDepends")
@Slf4j
public class ComponentDependController extends GenericProjectController<JsonNode>{

    private final ComponentDependService componentDependService;

    public ComponentDependController(final ComponentDependService componentDependService) {
        this.componentDependService = componentDependService;
        this.init(componentDependService);
    }

    @PatchMapping("/update")
    public ResponseEntity<JsonNode> updateFieldOfComponentDepends(@RequestBody JsonNode updateComponentDepends) {
        try {
            return ResponseEntity.ok(componentDependService.updateComponentDepends(updateComponentDepends));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

}
