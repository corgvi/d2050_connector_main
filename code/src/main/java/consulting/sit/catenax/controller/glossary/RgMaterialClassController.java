package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.RgMaterialClassService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rgMaterialClasses")
@Slf4j
public class RgMaterialClassController extends GenericController<JsonNode> {
    private final RgMaterialClassService rgMaterialClassService;

    public RgMaterialClassController(RgMaterialClassService rgMaterialClassService) {
        this.rgMaterialClassService = rgMaterialClassService;
        this.init(rgMaterialClassService);
    }
}
