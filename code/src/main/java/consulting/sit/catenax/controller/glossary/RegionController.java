package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.RegionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/regions")
@Slf4j
public class RegionController extends GenericController<JsonNode> {
    private final RegionService regionService;

    public RegionController(RegionService regionService) {
        this.regionService = regionService;
        this.init(regionService);
    }
}
