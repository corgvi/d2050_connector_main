package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.RqTreatmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rqTreatments")
@Slf4j
public class RqTreatmentController extends GenericController<JsonNode> {
    private final RqTreatmentService rqTreatmentService;

    public RqTreatmentController(RqTreatmentService rqTreatmentService) {
        this.rqTreatmentService = rqTreatmentService;
        this.init(rqTreatmentService);
    }
}
