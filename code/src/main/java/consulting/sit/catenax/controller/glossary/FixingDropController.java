package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.FixingDropService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fixingDrops")
@Slf4j
public class FixingDropController extends GenericController<JsonNode>{
    private final FixingDropService fixingDropService;

    public FixingDropController(final FixingDropService fixingDropService) {
        this.fixingDropService = fixingDropService;
        this.init(fixingDropService);
    }
}
