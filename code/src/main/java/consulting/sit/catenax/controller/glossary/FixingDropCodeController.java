package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.FixingDropCodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fixingDropCodes")
@Slf4j
public class FixingDropCodeController extends GenericController<JsonNode> {
    private final FixingDropCodeService fixingDropCodeService;

    public FixingDropCodeController(final FixingDropCodeService fixingDropCodeService) {
        this.fixingDropCodeService = fixingDropCodeService;
        this.init(fixingDropCodeService);
    }
}
