package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.KeycloakAuthenticationService;
import consulting.sit.catenax.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/projects")
@Slf4j
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;

    @GetMapping
    public ResponseEntity<JsonNode> getProjects() {
//        KeycloakAuthenticationToken authentication = (KeycloakAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
//        KeycloakSecurityContext keycloakContext = authentication.getAccount().getKeycloakSecurityContext();
//
//        // Retrieve user information from the KeycloakSecurityContext object
//        String username = keycloakContext.getToken().getPreferredUsername();
//        String email = keycloakContext.getToken().getEmail();
//        String fullName = keycloakContext.getToken().getPreferredUsername();
//        Set<String> roles = keycloakContext.getToken().getRealmAccess().getRoles();
//        log.info("----------------------------------------------------------------------------------");
//        log.info("Keycloak token string::" + keycloakContext.getTokenString());
//        log.info("Keycloak preferred Username::" + keycloakContext.getToken().getPreferredUsername());
//        log.info("Issuer::" + keycloakContext.getToken().getIssuer());
//        log.info("Issues for::" + keycloakContext.getToken().getIssuedFor());
//        log.info("Allowed origins::" + keycloakContext.getToken().getAllowedOrigins().stream().collect(Collectors.joining(" ")));

        return ResponseEntity.ok(projectService.getProjects());
    }

    @GetMapping("/{id}/variants")
    public ResponseEntity<JsonNode> getVariants(@PathVariable int id) {
        return ResponseEntity.ok(projectService.getVariants(id));
    }
    
    @GetMapping("/{id}/{language}/details")
    public ResponseEntity<JsonNode> getProjectDetails(@PathVariable int id, @PathVariable String language) {
        JsonNode jsonNode = projectService.getProjectDetails(id, language);
        if (jsonNode == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(jsonNode);
    }
    
    @PostMapping("/project")
    public ResponseEntity<JsonNode> newProject(@RequestBody JsonNode newProject) {
        try {
            return ResponseEntity.ok(projectService.newProject(newProject));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<JsonNode> updateFieldOfProject(@RequestBody JsonNode updateProject, @PathVariable String id) {
        try {
            return ResponseEntity.ok(projectService.updateProjectByField(updateProject, id));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<JsonNode> updateProject(@RequestBody JsonNode updateProject, @PathVariable String id) {
        try {
            return ResponseEntity.ok(projectService.updateProject(updateProject, id));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<JsonNode> deleteProject(@PathVariable String id) {
        try {
            projectService.deleteProject(id);
            return ResponseEntity.ok().build();
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.noContent().build();
        }

    }
}
