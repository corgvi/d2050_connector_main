package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;

import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.VariantDetailService;
import consulting.sit.catenax.service.VariantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/variantDetail")
@Slf4j
public class VariantDetailController {
    @Autowired
    private VariantDetailService variantDetailService;

    @GetMapping("/{id}")
    public ResponseEntity<JsonNode> getVariantDetail(@PathVariable int id) {
        JsonNode jsonNode = variantDetailService.getVariantDetailById(id, "en");
        if (jsonNode == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(jsonNode);
    }

    @PutMapping("/{id}")
    public ResponseEntity<JsonNode> updateVariantDetail(@RequestBody JsonNode updateVariantDetail, @PathVariable String id) {
        try {
            return ResponseEntity.ok(variantDetailService.updateVariantDetail(updateVariantDetail, id));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }    }

    @PatchMapping("/{id}")
    public ResponseEntity<JsonNode> updateFieldOfVariantDetail(@RequestBody JsonNode updateVariantDetail, @PathVariable String id) {
        try {
            return ResponseEntity.ok(variantDetailService.updateVariantDetailByField(updateVariantDetail, id));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping()
    public ResponseEntity<JsonNode> newVariantDetail(@RequestBody JsonNode newVariantDetail) {
        try {
            return ResponseEntity.ok(variantDetailService.newVariantDetail(newVariantDetail));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<JsonNode> deleteVariantDetail(@PathVariable String id) {
        try {
            variantDetailService.deleteVariantDetail(id);
            return ResponseEntity.ok().build();
        } catch (ExceptionDeleteProject ex) {
            return ResponseEntity.noContent().build();
        }

    }
}
