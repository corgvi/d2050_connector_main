package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.VariantTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/variantTypes")
@Slf4j
public class VariantTypeController extends GenericController<JsonNode> {
    private final VariantTypeService variantTypeService;

    public VariantTypeController(VariantTypeService variantTypeService) {
        this.variantTypeService = variantTypeService;
        this.init(variantTypeService);
    }
}
