package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.GlossaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/glossary")
@Slf4j
public class GlossaryController {
    private final GlossaryService glossaryService;

    public GlossaryController(final GlossaryService glossaryService) {
        this.glossaryService = glossaryService;
    }

    @GetMapping()
    public ResponseEntity<JsonNode> getAllComponentByVariantId() {
        return ResponseEntity.ok(glossaryService.getAllTableName());
    }

    @GetMapping("/{tableName}")
    public ResponseEntity<JsonNode> getAllComponentByVariantId(@PathVariable String tableName) {
        try {
            return ResponseEntity.ok(glossaryService.getDateModelByTableName(tableName));
        } catch (HttpClientErrorException e){
            return ResponseEntity.notFound().build();
        }
    }
}
