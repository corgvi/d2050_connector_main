package consulting.sit.catenax.controller;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.ComponentCompService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/componentComps")
@Slf4j
public class ComponentCompController extends GenericProjectController<JsonNode>{

    private final ComponentCompService componentCompService;

    public ComponentCompController(final ComponentCompService componentCompService) {
        this.componentCompService = componentCompService;
        this.init(componentCompService);
    }

//    @PatchMapping("/{id}")
//    public ResponseEntity<JsonNode> updateFieldOfComponentComp(@RequestBody JsonNode updateComponentComp, @PathVariable String id) {
//        try {
//            return ResponseEntity.ok(componentCompService.updateComponentCompByField(updateComponentComp, id));
//        } catch (HttpClientErrorException ex) {
//            return ResponseEntity.badRequest().build();
//        }
//    }

    @PatchMapping("/update")
    public ResponseEntity<JsonNode> updateFieldOfComponentComps(@RequestBody JsonNode updateComponentComp) {
        try {
            return ResponseEntity.ok(componentCompService.updateComponentComps(updateComponentComp));
        } catch (HttpClientErrorException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

}
