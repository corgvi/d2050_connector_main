package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.MaterialCharService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/materialChars")
@Slf4j
public class MaterialCharController extends GenericController<JsonNode> {
    private final MaterialCharService materialCharService;

    public MaterialCharController(final MaterialCharService materialCharService) {
        this.materialCharService = materialCharService;
        this.init(materialCharService);
    }
}
