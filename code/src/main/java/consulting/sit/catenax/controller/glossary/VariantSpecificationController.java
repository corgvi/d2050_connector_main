package consulting.sit.catenax.controller.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.service.glossary.VariantSpecificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/variantSpecifications")
@Slf4j
public class VariantSpecificationController extends GenericController<JsonNode> {
    private final VariantSpecificationService variantSpecificationService;

    public VariantSpecificationController(VariantSpecificationService variantSpecificationService) {
        this.variantSpecificationService = variantSpecificationService;
        this.init(variantSpecificationService);
    }
}
