package consulting.sit.catenax.controller.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.glossary.GenericController;
import consulting.sit.catenax.service.glossary.FixingMainService;
import consulting.sit.catenax.service.glossary.languageMap.FixingMainLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fixingMainLanguageMaps")
@Slf4j
public class FixingMainLanguageMapController extends GenericLanguageMapController<JsonNode> {
    private final FixingMainLanguageMapService fixingMainLanguageMap;

    public FixingMainLanguageMapController(final FixingMainLanguageMapService fixingMainLanguageMap) {
        this.fixingMainLanguageMap = fixingMainLanguageMap;
        this.init(fixingMainLanguageMap);
    }

}
