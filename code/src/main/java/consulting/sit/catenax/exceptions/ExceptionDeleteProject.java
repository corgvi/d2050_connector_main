package consulting.sit.catenax.exceptions;

public class ExceptionDeleteProject extends RuntimeException{
    private static final long serialVersionUID = 7433205024128251103L;

    public ExceptionDeleteProject(String exceptionMessage) {
        super(exceptionMessage);
    }
}
