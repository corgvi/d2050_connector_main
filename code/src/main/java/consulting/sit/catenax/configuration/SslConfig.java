//package consulting.sit.catenax.configuration;
//
//import lombok.Getter;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//
//import javax.annotation.PostConstruct;
//
//@Configuration
//@Slf4j
//public class SslConfig {
//    private static final String TRUST_STORE = "javax.net.ssl.trustStore";
//    private static final String TRUST_STORE_PASSWORD = "javax.net.ssl.trustStorePassword";
//
//    @Value("${server.ssl.trust-store}")
//    @Getter
//    private String trustStore;
//
//    @Value("${server.ssl.trust-store-password}")
//    @Getter
//    private String trustStorePassword;
//
//    @PostConstruct
//    private void configureSSL() {
//        //set to TLSv1.1 or TLSv1.2
//        System.setProperty("https.protocols", "TLSv1.2");
//
//        //load the 'javax.net.ssl.trustStore' and
//        //'javax.net.ssl.trustStorePassword' from application.properties
//        if (System.getProperty(TRUST_STORE) == null) {
//            System.setProperty(TRUST_STORE, trustStore);
//        }
//
//        if (System.getProperty(TRUST_STORE_PASSWORD) == null) {
//            System.setProperty(TRUST_STORE_PASSWORD, trustStorePassword);
//        }
//
//        log.debug("{}: {}", TRUST_STORE, System.getProperty(TRUST_STORE));
//        log.debug("{}: {}", TRUST_STORE_PASSWORD, System.getProperty(TRUST_STORE_PASSWORD));
//    }
//}
//
