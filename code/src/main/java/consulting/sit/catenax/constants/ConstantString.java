package consulting.sit.catenax.constants;

public class ConstantString {
    private ConstantString() {
    }

    public static final String COMPONENT_MAIN_BENENNUNG_ENTITY = "componentMainBenennungEntity";
    public static final String VARIANT_DETAIL_ENTITY = "variantDetailEntity";

    public static final String VARIANT_AIRBAG = "variantAirbags";
    public static final String MATERIAL_MAIN_ENTITY = "materialMainEntity";
    public static final String MATERIAL_GROUP_ENTITY = "materialGroupEntity";
    public static final String CODE = "code";
    public static final String MATERIAL_MAIN_LANGUAGE_MAPS = "materialMainLanguageMaps";
    public static final String PART_LANGUAGE_MAPS = "partLanguageMaps";

    public static final String ID = "id";
    public static final String PROJECT_ID = "projectId";
    public static final String VARIANT_ID = "variantId";
    public static final String CREATED_BY = "createdBy";
    public static final String CREATED_AT = "createdAt";
    public static final String MODIFIED_BY = "modifiedBy";
    public static final String MODIFIED_AT = "modifiedAt";
    public static final String DESCRIPTION = "description";
    public static final String NAME = "name";
    public static final String COMPONENT_ID = "componentId";
    public static final String BENENNUNG_ORIGINAL = "benennungOrginal";
    public static final String LEVEL_IN_BOM = "levelInBom";
    public static final String TOP = "top";
    public static final String WEIGHT = "weight";
    public static final String MASSE = "masse";
    public static final String ABHAENGIGKEIT = "abhaengigkeit";
    public static final String VALUE = "value";
    public static final String CHILDREN = "children";
    public static final String LANGUAGE = "language";
    public static final String FAHRZEUG_BEZEICHNUNG = "fahrzeugbezeichnung";
    public static final String DISMANTLINGSTUDY = "dismantlingStudy";
    public static final String UNTERSUCHUNGSTART = "untersuchungStart";
    public static final String UNTERSUCHUNGENDE = "untersuchungEnde";
    public static final String BATTERIETYP12V = "batterieTyp12v";
    public static final String BATTERIETYPHV = "batterieTypHv";
    public static final String BATTERIETYP24V = "batterieTyp24v";
    public static final String BATTERIETYP48V = "batterieTyp48v";
    public static final String VERSION = "version";
    public static final String CXPROJECT = "cxProject";
    public static final String VARIANT = "variant";
    public static final String TYPE = "type";
    public static final String COMPONENT = "Component";
    public static final String SEMI_COMPONENT = "Semi-component";
    public static final String MATERIAL = "Material";
    public static final String SUBSTANCE = "Substance";
    public static final String ACTIVITY_AREA_LANGUAGE_MAPS = "activityAreaLanguageMaps";
    public static final String ACTIVITY_AREA_LANGUAGE_MAP_PK = "activityAreaLanguageMapPk";
    public static final String MATERIAL_MAIN_LANGUAGE_MAP_PK = "materialMainLanguageMapPk";
    public static final String COMPOUND_LANGUAGE_MAP_PK = "compoundLanguageMapPk";
    public static final String FRACTION_MAIN_LANGUAGE_MAP_PK = "fractionMainLanguageMapPk";
    public static final String MARK_IDENT_LANGUAGE_MAP_PK = "markIdentLanguageMapPk";
    public static final String PART_LANGUAGE_MAP_PK = "partLanguageMapPk";
    public static final String TOOL_MAIN_LANGUAGE_MAP_PK = "toolMainLanguageMapPk";
    public static final String FIXING_MAIN_LANGUAGE_MAP_PK = "fixingMainLanguageMapPk";
    public static final String FIXING_LOCATION_LANGUAGE_MAP_PK = "fixingLocationLanguageMapPk";
    public static final String FIXING_DROP_LANGUAGE_MAP_PK = "fixingDropLanguageMapPk";
    public static final String FIXING_DROP_CODE_LANGUAGE_MAP_PK = "fixingDropCodeLanguageMapPk";
    public static final String BESCHREIBUNG = "beschreibung";
    public static final String XLAGEVE = "xlageVe";
    public static final String ZBTEIL = "zbTeil";
    public static final String KONSTRUKTIV = "konstruktiv";
    public static final String STRATEGISCH = "strategisch";
    public static final String ZBPART = "zbPart";
    public static final String AUSBAUPFLICHT = "ausbaupflicht";
    public static final String VARIANTE = "variante";
    public static final String SA = "sa";
    public static final String DIREKT = "direkt";
    public static final String BAGCOATED = "bagCoated";
    public static final String BAGMARKED = "bagMarked";
    public static final String VERGLEICHSTEIL = "vergleichsteil";
    public static final String VERBUNDANTEIL = "verbundanteil";
    public static final String EINSATZ = "einsatz";




}
