package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import consulting.sit.catenax.service.glossary.GenericService;
import io.swagger.v3.core.util.Json;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Service
public class FixingService extends GenericProjectService<JsonNode> {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    private final String URI_FIXING = "project/fixing";
    private final String URI_COMPONENT_MAIN = "project/componentMain";

    @Override
    protected String getUrl() {
        return URI_FIXING;
    }


    public JsonNode updateFixings(JsonNode fixings) {
        try {
            if (!fixings.isEmpty()) {
                if (fixings.hasNonNull(ConstantString.COMPONENT_ID) && fixings.get(ConstantString.COMPONENT_ID).asText() != null) {
                    String componentId = fixings.get(ConstantString.COMPONENT_ID).asText();
                    final String uriComponentMain = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_MAIN).append("/").append(componentId).toString();
                    JsonNode componentMain = restTemplate.getForObject(new URI(uriComponentMain), JsonNode.class);
                    if (componentMain != null) {
                        ObjectMapper mapper = new ObjectMapper();
                        ArrayNode component = mapper.createArrayNode();
                        JsonNode fixing = fixings.get("fixings");
                        if (fixing == null) {
                            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                        }
                        fixing.forEach(cd -> {
                            if (cd != null) {
                                if (cd.hasNonNull(ConstantString.ID) && cd.get(ConstantString.ID).asText() != null) {
                                    final String fixingId = cd.get(ConstantString.ID).asText();
                                    JsonNode newFixing = updateByField(cd, fixingId);
                                    component.add(newFixing);
                                }
                                if (!cd.hasNonNull(ConstantString.ID)) {
                                    JsonNode newFixing = createFixing(cd, componentId);
                                    component.add(newFixing);
                                }
                            }
                        });
                        return component;
                    } else {
                        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
                    }
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }

            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private JsonNode createFixing(final JsonNode fixing, final String componentMainId) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_FIXING).toString();
        final KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            ObjectNode fixingNode = (ObjectNode) fixing;
            if (fixingNode == null) {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }

            if (!Objects.isNull(keycloakSecurityContext)) {
                fixingNode.put(ConstantString.CREATED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                fixingNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
            fixingNode.put(ConstantString.COMPONENT_ID, componentMainId);
            fixingNode.put(ConstantString.EINSATZ, jsonNodeModificationHelper.checkBool(fixingNode, ConstantString.EINSATZ));
            JsonNode newComp = restTemplate.postForObject(new URI(uri), fixingNode, JsonNode.class);
            return newComp;

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }

    }
    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        if (createNode.get(ConstantString.COMPONENT_ID) != null && createNode.hasNonNull(ConstantString.COMPONENT_ID)) {
            String componentId = createNode.get(ConstantString.COMPONENT_ID).asText();
            final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_MAIN).append("/").append(componentId).toString();
            try {
                JsonNode component = restTemplate.getForObject(new URI(uri), JsonNode.class);
                if (component != null) {
                    createNode.put(ConstantString.EINSATZ, jsonNodeModificationHelper.checkBool(createNode, ConstantString.EINSATZ));
                    return createNode;
                }
                else throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }

        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }

    }

}
