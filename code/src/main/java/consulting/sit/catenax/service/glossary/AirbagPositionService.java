package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class AirbagPositionService extends GenericService<JsonNode> {

    private final String URI_AIRBAG_POSITION = "glossary/airbagPosition";

    @Override
    protected String getUrl() {
        return URI_AIRBAG_POSITION;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }

}
