package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import io.swagger.v3.core.util.Json;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Service
public abstract class GenericProjectService<T> {
    @Autowired
    private RestTemplate restTemplate;
    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;


    protected abstract String getUrl();

    public T create(T newObject) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).toString();
        final KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            if (!Objects.isNull(newObject)) {
                ObjectNode node = (ObjectNode) newObject;
                if (!Objects.isNull(keycloakSecurityContext)) {
                    node.put(ConstantString.CREATED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                    node.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                    CreateObject(node);
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }
                JsonNode newNode = restTemplate.postForObject(new URI(uri), node, JsonNode.class);
                return (T) newNode;
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected abstract JsonNode CreateObject(ObjectNode createNode);

    public void delete(final String id) throws ExceptionDeleteProject {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).append("/").append(id).toString();
        try {
            restTemplate.delete(new URI(uri));
        } catch (URISyntaxException | HttpServerErrorException ex) {
            ex.printStackTrace();
            throw new ExceptionDeleteProject("Error when delete project");
        }
    }

    public T updateByField(final JsonNode updateObject, final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).append("/").append(id).toString();
        final KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode oldObject = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (!oldObject.isNull()) {
                ObjectNode node = (ObjectNode) updateObject;
                if (!Objects.isNull(keycloakSecurityContext)) {
                    node.put(ConstantString.CREATED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                    node.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                    restTemplate.patchForObject(new URI(uri), node, JsonNode.class);
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }

                try {
                    JsonNode newObject = restTemplate.getForObject(new URI(uri), JsonNode.class);
                    return (T) newObject;
                } catch (HttpClientErrorException e) {
                    throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
                }
            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }

    }
}
