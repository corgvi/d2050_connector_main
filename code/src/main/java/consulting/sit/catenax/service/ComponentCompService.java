package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.KeycloakAuthenticationService;
import io.swagger.v3.core.util.Json;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

@Service
public class ComponentCompService extends GenericProjectService<JsonNode> {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    private final String URI_COMPONENT_COMP = "project/componentComp";
    private final String URI_COMPONENT_MAIN = "project/componentMain";

//    private JsonNode updateComponentCompByField(final JsonNode updateComponentComp, final String id) {
//        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_COMP).append("/").append(id).toString();
//        final KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
//        try {
//            JsonNode oldComponentCompUpdate = restTemplate.getForObject(new URI(uri), JsonNode.class);
//            if (!oldComponentCompUpdate.isNull()) {
//                ObjectNode componentCompNode = (ObjectNode) updateComponentComp;
//                if (!Objects.isNull(keycloakSecurityContext)) {
//                    componentCompNode.put(ConstantString.CREATED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
//                    componentCompNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
//                    restTemplate.patchForObject(new URI(uri), componentCompNode, JsonNode.class);
//                } else {
//                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
//                }
//
//                try {
//                    JsonNode newCompUpdate = restTemplate.getForObject(new URI(uri), JsonNode.class);
//                    return newCompUpdate;
//                } catch (HttpClientErrorException e) {
//                    throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
//                }
//            } else {
//                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
//            }
//
//        } catch (URISyntaxException ex) {
//            ex.printStackTrace();
//            return null;
//        }
//
//    }

    public JsonNode updateComponentComps(JsonNode componentComps) {
        try {
            if (!componentComps.isEmpty()) {
                if (componentComps.hasNonNull(ConstantString.COMPONENT_ID) && componentComps.get(ConstantString.COMPONENT_ID).asText() != null) {
                    String componentId = componentComps.get(ConstantString.COMPONENT_ID).asText();
                    final String uriComponentMain = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_MAIN).append("/").append(componentId).toString();
                    JsonNode componentMain = restTemplate.getForObject(new URI(uriComponentMain), JsonNode.class);
                    if (componentMain != null) {
                        ObjectMapper mapper = new ObjectMapper();
                        ArrayNode component = mapper.createArrayNode();
                        JsonNode componentComp = componentComps.get("componentComps");
                        if (componentComp == null) {
                            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                        }
                        componentComp.forEach(cd -> {
                            if (cd != null) {
                                if (cd.hasNonNull(ConstantString.ID) && cd.get(ConstantString.ID).asText() != null) {
                                    final String componentCompId = cd.get(ConstantString.ID).asText();
                                    JsonNode newComponentComp = updateByField(cd, componentCompId);
                                    component.add(newComponentComp);
                                }
                                if (!cd.hasNonNull(ConstantString.ID)) {
                                    JsonNode newComponentComp = createComponentComp(cd, componentId);
                                    component.add(newComponentComp);
                                }
                            }
                        });
                        return component;
                    } else {
                        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
                    }
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }

            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private JsonNode createComponentComp(final JsonNode componentComp, final String componentMainId) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_COMP).toString();
        final KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            ObjectNode componentCompNode = (ObjectNode) componentComp;
            if (componentCompNode == null) {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }

            if (!Objects.isNull(keycloakSecurityContext)) {
                componentCompNode.put(ConstantString.CREATED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                componentCompNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
            componentCompNode.put("componentMainId", componentMainId);
            JsonNode newComp = restTemplate.postForObject(new URI(uri), componentCompNode, JsonNode.class);
            return newComp;

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }

    }
    @Override
    protected String getUrl() {
        return URI_COMPONENT_COMP;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        if (createNode.get("componentMainId").asText() != null && createNode.hasNonNull("componentMainId")) {
            return createNode;
        }
        else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }
}
