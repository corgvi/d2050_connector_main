package consulting.sit.catenax.service;

import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class KeycloakAuthenticationService {

    public KeycloakAuthenticationToken getKeycloakAuthenticationToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof KeycloakAuthenticationToken) {
            return (KeycloakAuthenticationToken) authentication;
        }
        return null;
    }

    public KeycloakSecurityContext getKeycloakSecurityContext() {
        if (!Objects.isNull(getKeycloakAuthenticationToken())) {
            return getKeycloakAuthenticationToken().getAccount().getKeycloakSecurityContext();
        } else {
            return null;
        }
    }


}