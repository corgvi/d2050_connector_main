package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class RgIsoGroupService extends GenericService<JsonNode> {

    private final String URI_RG_ISO_GROUP = "glossary/rgIsoGroup";

    @Override
    protected String getUrl() {
        return URI_RG_ISO_GROUP;
    }


    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
