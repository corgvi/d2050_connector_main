package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import io.swagger.v3.core.util.Json;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ComponentService extends GenericProjectService<JsonNode> {
    private static final Logger LOGGER = LogManager.getLogger(ComponentService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private VariantService variantService;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    private final String URI_COMPONENT_MAIN = "project/componentMain";
    private final String URI_COMPONENT_DEPEND = "project/componentDepend";

    public JsonNode getTree(Integer variantId, String language) {
        JsonNode variantNode = variantService.getVariantDetails(variantId, language);
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resultNode = mapper.createObjectNode();
        resultNode.put(ConstantString.ID, variantId);
        resultNode.set(ConstantString.NAME, variantNode.get("name"));
        resultNode.put("type", "variant");
        if (variantNode.hasNonNull(ConstantString.PROJECT_ID) && variantNode.get(ConstantString.PROJECT_ID).asText() != null) {
            String projectId = variantNode.get(ConstantString.PROJECT_ID).asText();
            final String uriProject = d2050ServiceBaseurl + "project/project/" + projectId;
            try {
                JsonNode projectNode = restTemplate.getForObject(new URI(uriProject), JsonNode.class);
                resultNode.put("projectName", projectNode.get(ConstantString.NAME).asText());
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }

        List<Long> rootComponentIdList = getTopComponentFromVariant(variantId);
        // TODO: ggf. mehrere Root-Components abfragen
        ArrayNode children = mapper.createArrayNode();
        // TODO: Get root componentId from variant
        // TODO: Remove usage of variantID as first componentId
        rootComponentIdList.forEach(componentId -> {
            if (componentId != null) {
                children.add(getChildren(componentId, language));
            }
        });

        resultNode.set(ConstantString.CHILDREN, children);

        return resultNode;
    }

    public JsonNode getFirstChildOfTree(Integer variantId, String language) {
        JsonNode variantNode = variantService.getVariantDetails(variantId, language);
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resultNode = mapper.createObjectNode();
        resultNode.put(ConstantString.ID, variantId);
        resultNode.set(ConstantString.NAME, variantNode.get(ConstantString.NAME));
        resultNode.put(ConstantString.TYPE, ConstantString.VARIANT);

        List<Long> rootComponentIdList = getTopComponentFromVariant(variantId);
        // TODO: ggf. mehrere Root-Components abfragen
        ArrayNode children = mapper.createArrayNode();
        // TODO: Get root componentId from variant
        // TODO: Remove usage of variantID as first componentId
        rootComponentIdList.forEach(componentId -> {
            if (componentId != null) {
                children.add(getChildrenFirstOfComponent(componentId, language, ConstantString.COMPONENT));
            }
        });

        resultNode.set(ConstantString.CHILDREN, children);

        return resultNode;
    }

    private List<Long> getTopComponentFromVariant(int variantId) {
        List<Long> resultList = new ArrayList<>();
        String componentUri = d2050ServiceBaseurl + "project/componentMain/variant/" + variantId + "/first";
        try {
            JsonNode componentNode = restTemplate.getForObject(new URI(componentUri), JsonNode.class);
            if (componentNode != null) {
                componentNode.forEach(e -> resultList.add(e.get(ConstantString.ID).asLong()));
            }
        } catch (URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return resultList;
    }

    private JsonNode getChildrenFirstOfComponent(long componentId, String language, String type) {
        // Request URIs
        final String uriComponent = d2050ServiceBaseurl + "project/componentMain/" + componentId;
        try {
            // request Component
            JsonNode component = restTemplate.getForObject(new URI(uriComponent), JsonNode.class);
            if (component != null) {
                // Create children array
                ObjectMapper mapper = new ObjectMapper();
                // Join Elements to JsonNode
                ObjectNode returnNode = mapper.createObjectNode();
                returnNode.put(ConstantString.ID, getId(component));
                returnNode.put(ConstantString.NAME, getName(component, language));
                returnNode.put(ConstantString.TYPE, type);

                return returnNode;
            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public JsonNode getChildren(long componentId, String language) {
        // Request URIs
        final String uriComponent = d2050ServiceBaseurl + "project/componentMain/" + componentId;
        final String uri = d2050ServiceBaseurl + "project/componentMain/children/" + componentId;
        try {
            // request Component
            JsonNode component = restTemplate.getForObject(new URI(uriComponent), JsonNode.class);
            // Create children array
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode children = mapper.createArrayNode();

            // request children
            JsonNode componentTree = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (componentTree != null) {
                componentTree.forEach(cd -> {
                    if (cd != null
                            && cd.get(ConstantString.ID) != null) {
                        children.add(getChildren(cd.get(ConstantString.ID).asLong(), language));
                    }
                });
            }


            // Material to children array
            if (component != null
                    && component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY) != null
                    && component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS) != null
                    && component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language) != null) {
                ObjectNode materialNode = mapper.createObjectNode();
                materialNode.put(ConstantString.ID, component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.ID).asLong());
                materialNode.set(ConstantString.NAME, component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE));
                materialNode.put(ConstantString.TYPE, ConstantString.MATERIAL);
                materialNode.set(ConstantString.CHILDREN, mapper.createArrayNode());
                children.add(materialNode);
            }

            // Join Elements to JsonNode
            ObjectNode returnNode = mapper.createObjectNode();
            returnNode.put(ConstantString.ID, getId(component));
            returnNode.put(ConstantString.NAME, getName(component, language));
            returnNode.put(ConstantString.TYPE, ConstantString.COMPONENT);
            returnNode.set(ConstantString.CHILDREN, children);

            return returnNode;
        } catch (URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public JsonNode getChildrenByComponentIdAndLevelOfTree(long componentId, String language, int level) {
        // Request URIs
        final String uriComponent = d2050ServiceBaseurl + "project/componentMain/" + componentId;
        final String uri = d2050ServiceBaseurl + "project/componentMain/children/" + componentId;


        try {
            // request Component
            JsonNode component = restTemplate.getForObject(new URI(uriComponent), JsonNode.class);

            // Create children array
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode children = mapper.createArrayNode();
            ObjectNode returnNode = mapper.createObjectNode();
            // Material to children array
            if (component != null
                    && component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY) != null
                    && component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS) != null
                    && component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language) != null) {
                ObjectNode materialNode = mapper.createObjectNode();
                materialNode.put(ConstantString.ID, component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.ID).asLong());
                materialNode.set(ConstantString.NAME, component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE));
                materialNode.put(ConstantString.TYPE, ConstantString.MATERIAL);
                materialNode.set(ConstantString.CHILDREN, mapper.createArrayNode());
                children.add(materialNode);
            }
            // Join Elements to JsonNode
            returnNode.put(ConstantString.ID, getId(component));
            returnNode.put(ConstantString.NAME, getName(component, language));
            returnNode.put(ConstantString.TYPE, ConstantString.COMPONENT);
            returnNode.set(ConstantString.CHILDREN, children);


            // request children
            JsonNode componentChildren = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (componentChildren != null) {
                if (level <= 0) {
                    return returnNode;
                }
                final int levelOfComponent = level - 1;
                if (levelOfComponent < 0) {
                    return returnNode;
                } else {
                    componentChildren.forEach(cd -> {
                        if (cd != null
                                && cd.get(ConstantString.ID) != null) {
                            children.add(getChildrenByComponentIdAndLevelOfTree(cd.get(ConstantString.ID).asLong(), language, levelOfComponent));
                        }
                    });
                }
            }

            return returnNode;
        } catch (URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Returns name of component.
     * Check componentMainBenennungEntity, if it is not existing return benennungOriginal.
     *
     * @param component JsonNode to look for the componentName
     * @param language  Language you want to return the componentName
     * @return componentName from componentMainBenennungEntity in given language or if not found benennungOriginal
     */
    private String getName(JsonNode component, String language) {
        if (component != null) {
            if (component.hasNonNull(ConstantString.NAME)) {
                if (component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY) != null
                        && component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS) != null
                        && component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language) != null
                        && component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE) != null) {
                    return component.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE).asText();
                } else {
                    return component.get(ConstantString.NAME).asText();
                }
            } else {
                return "NO NAME";
            }
        } else {
            return null;
        }
    }

    /**
     * Returns id of component.
     * Check if componentId exists and return it.
     *
     * @param component JsonNode to look for the id
     * @return id from the given component, null if componentId is not found
     */
    private Long getId(JsonNode component) {
        if (component != null
                && component.get(ConstantString.ID) != null) {
            return component.get(ConstantString.ID).asLong();
        }
        return null;
    }

    private String getProjectName(JsonNode node) {
        if (node != null) {
            if (node.hasNonNull("variantEntity")
                    && node.get("variantEntity").get(ConstantString.PROJECT_ID).asText() != null) {
                String projectId = node.get("variantEntity").get(ConstantString.PROJECT_ID).asText();
                final String uriProject = d2050ServiceBaseurl + "project/project/" + projectId;

                try {
                    JsonNode projectNode = restTemplate.getForObject(new URI(uriProject), JsonNode.class);
                    return projectNode.get(ConstantString.NAME).asText();
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            } else throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        } else throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
    }

    public JsonNode getComponentDetail(int componentId, String language) {
//        ObjectMapper mapper = new ObjectMapper();
//        ObjectNode resultNode = mapper.createObjectNode();
        final String uriComponent = d2050ServiceBaseurl + "project/componentMain/" + componentId;

        try {
            JsonNode componentNode = restTemplate.getForObject(new URI(uriComponent), JsonNode.class);
            ObjectNode resultNode = (ObjectNode) componentNode;
            resultNode.put("projectName", getProjectName(resultNode));
//            if (resultNode != null) {
//                resultNode.put(ConstantString.ID, getId(componentNode));
//                resultNode.put(ConstantString.NAME, getName(componentNode, language));
//                resultNode.set(ConstantString.WEIGHT, componentNode.get(ConstantString.MASSE));
//                resultNode.set(ConstantString.TOP, componentNode.get(ConstantString.TOP));
//                resultNode.set(ConstantString.LEVEL_IN_BOM, componentNode.get(ConstantString.LEVEL_IN_BOM)); // different spelling, consider harmonization
//                resultNode.set(ConstantString.BENENNUNG_ORIGINAL, componentNode.get(ConstantString.BENENNUNG_ORIGINAL));
//            }
            return componentNode;
        } catch (URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    private String checkFieldBool(JsonNode jsonNode, String fieldName) {
        if (jsonNode.get(fieldName) != null && jsonNode.hasNonNull(fieldName)) {
            return jsonNode.get(fieldName).asText();
        } else return "false";
    }

    public JsonNode getAllAssemblyPart(String language) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_MAIN).append("/zbteil").toString();
        try {
            JsonNode component = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (component != null) {
                ObjectMapper mapper = new ObjectMapper();
                ArrayNode returnNode = mapper.createArrayNode();
                component.forEach(currentNode -> {
                    ObjectNode objectNode = mapper.createObjectNode();
                    objectNode.set(ConstantString.ID, currentNode.get(ConstantString.ID));
                    objectNode.set(ConstantString.ZBTEIL, currentNode.get(ConstantString.ZBTEIL));

                    if (currentNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY) != null
                            && currentNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS) != null
                            && currentNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language) != null
                            && currentNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE) != null) {
                        objectNode.set(ConstantString.NAME, currentNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE));
                    } else {
                        objectNode.set(ConstantString.NAME, currentNode.get(ConstantString.NAME));
                    }

                    returnNode.add(objectNode);
                });
                return returnNode;
            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (URISyntaxException e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    public JsonNode getAllComponentDependByComponentId(int componentId) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_DEPEND).append("/componentMain/").append(componentId).toString();
        try {
            JsonNode component = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (component != null) {
                ObjectMapper mapper = new ObjectMapper();
                ArrayNode returnNode = mapper.createArrayNode();
                component.forEach(currentNode -> {
                    ObjectNode objectNode = mapper.createObjectNode();
                    objectNode.set(ConstantString.ID, currentNode.get(ConstantString.ID));
                    objectNode.set(ConstantString.COMPONENT_ID, currentNode.get(ConstantString.COMPONENT_ID));
                    objectNode.set(ConstantString.ABHAENGIGKEIT, currentNode.get(ConstantString.ABHAENGIGKEIT));
                    objectNode.set(ConstantString.KONSTRUKTIV, currentNode.get(ConstantString.KONSTRUKTIV));
                    objectNode.set(ConstantString.STRATEGISCH, currentNode.get(ConstantString.STRATEGISCH));
                    objectNode.set(ConstantString.ZBPART, currentNode.get(ConstantString.ZBPART));
                    returnNode.add(objectNode);
                });
                return returnNode;
            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (URISyntaxException e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    public JsonNode getAllComponentByVariant(int variantId, String language) {
        String componentUri = d2050ServiceBaseurl + "project/componentMain/variant/" + variantId;
        try {
            JsonNode componentNode = restTemplate.getForObject(new URI(componentUri), JsonNode.class);
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode returnNode = mapper.createArrayNode();
            if (componentNode != null) {
                componentNode.forEach(e -> {
                    ObjectNode node = mapper.createObjectNode();
                    node.set(ConstantString.ID, e.get(ConstantString.ID));
                    node.put(ConstantString.NAME, getName(e, language));
                    returnNode.add(node);
                });
            }
            return returnNode;
        } catch (URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }

    }

    @Override
    protected String getUrl() {
        return URI_COMPONENT_MAIN;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        createNode.put(ConstantString.ZBTEIL, checkFieldBool(createNode, ConstantString.ZBTEIL));
        createNode.put(ConstantString.AUSBAUPFLICHT, checkFieldBool(createNode, ConstantString.AUSBAUPFLICHT));
        createNode.put(ConstantString.VARIANTE, checkFieldBool(createNode, ConstantString.VARIANTE));
        createNode.put(ConstantString.VERGLEICHSTEIL, checkFieldBool(createNode, ConstantString.VERGLEICHSTEIL));
        createNode.put(ConstantString.SA, checkFieldBool(createNode, ConstantString.SA));
        createNode.put(ConstantString.DIREKT, checkFieldBool(createNode, ConstantString.DIREKT));
        createNode.put(ConstantString.BAGCOATED, checkFieldBool(createNode, ConstantString.BAGCOATED));
        createNode.put(ConstantString.BAGMARKED, checkFieldBool(createNode, ConstantString.BAGMARKED));
        if (createNode.get(ConstantString.LEVEL_IN_BOM) != null && createNode.hasNonNull(ConstantString.LEVEL_IN_BOM)) {
            createNode.put(ConstantString.LEVEL_IN_BOM, createNode.get(ConstantString.LEVEL_IN_BOM));
        } else {
            createNode.put(ConstantString.LEVEL_IN_BOM, 1);
        }
        if (createNode.get(ConstantString.TOP) != null && createNode.hasNonNull(ConstantString.TOP)) {
            String componentId = createNode.get(ConstantString.TOP).asText();
            String variantId = createNode.get(ConstantString.VARIANT_ID).asText();
            final String uri = d2050ServiceBaseurl + "project/componentMain/check/" + componentId + "/variant/" + variantId;
            try {
                Boolean check = restTemplate.getForObject(new URI(uri), Boolean.class);
                if (check) {
                    createNode.put(ConstantString.TOP, componentId);
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }

        return createNode;
    }
}
