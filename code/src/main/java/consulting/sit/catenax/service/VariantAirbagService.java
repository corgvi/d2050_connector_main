package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Service
public class VariantAirbagService {
    private static final Logger LOGGER = LogManager.getLogger(VariantAirbagService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;
    private static final String URI_VARIANT_AIRBAG = "project/variantAirbag";
    private static final String URI_AIRBAG_POSITION = "glossary/airbagPosition";
    private static final String URI_VARIANT = "project/variant";

    public JsonNode getVariantAirbagDetails(int id, String language) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_AIRBAG).append("/").append(id).toString();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode resultNode = mapper.createObjectNode();

            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (node != null) {
                String airbagPositionId = node.get("airbagPositionId").asText();
                if (!airbagPositionId.equals("null")) {
                    final String uriAirBagPosition = new StringBuilder().append(d2050ServiceBaseurl).append(URI_AIRBAG_POSITION).append("/").append(airbagPositionId).toString();
                    JsonNode airbagPosition = restTemplate.getForObject(new URI(uriAirBagPosition), JsonNode.class);
                    resultNode.put("airbagPosition", airbagPosition.get("bezeichnung").asText());
                }
                resultNode.put(ConstantString.ID, node.get(ConstantString.ID).asInt());
                resultNode.put(ConstantString.VARIANT_ID, node.get(ConstantString.VARIANT_ID).asInt());
                resultNode.put("existing", node.get("existing").asBoolean());
                resultNode.put("typ", node.get("typ").asText());
            }
            return resultNode;
        } catch (URISyntaxException | HttpClientErrorException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public JsonNode updateVariantAirbag(final JsonNode updateVariantAirbag, final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_AIRBAG).append("/").append(id).toString();
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode oldVariantAirbag = restTemplate.getForObject(new URI(uri), JsonNode.class);
            String variantId = oldVariantAirbag.get("variantId").asText();
            JsonNode variant = null;
            String uriVariant = "";
            if (variantId != null) {
                uriVariant = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).append("/").append(variantId).toString();
                variant = restTemplate.getForObject(new URI(uriVariant), JsonNode.class);
            }
                ObjectNode variantNode = (ObjectNode) variant;
                if (!oldVariantAirbag.isNull()) {
                    if (!Objects.isNull(keycloakSecurityContext)) {
                        if (variantNode != null) {
                            int version = variant.get(ConstantString.VERSION).asInt();
                            version++;
                            variantNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                            variantNode.put(ConstantString.VERSION, version);
                        }
                    }
                    restTemplate.patchForObject(new URI(uriVariant), variantNode, JsonNode.class);
                    restTemplate.put(new URI(uri), updateVariantAirbag);
                    JsonNode newVariantAirbag = restTemplate.getForObject(new URI(uri), JsonNode.class);
                    return newVariantAirbag;
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }
    }



    public JsonNode updateVariantAirbagByField(final JsonNode updateVariantAirbag, final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_AIRBAG).append("/").append(id).toString();
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode oldVariantAirbag = restTemplate.getForObject(new URI(uri), JsonNode.class);
            String variantId = oldVariantAirbag.get("variantId").asText();
            JsonNode variant = null;
            String uriVariant = "";
            if (variantId != null) {
                uriVariant = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).append("/").append(variantId).toString();
                variant = restTemplate.getForObject(new URI(uriVariant), JsonNode.class);
            }
            ObjectNode variantNode = (ObjectNode) variant;
            if (!oldVariantAirbag.isNull()) {
                if (!Objects.isNull(keycloakSecurityContext)) {
                    if (variantNode != null) {
                        int version = variant.get(ConstantString.VERSION).asInt();
                        version++;
                        variantNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                        variantNode.put(ConstantString.VERSION, version);
                    }
                }
                restTemplate.patchForObject(new URI(uriVariant), variantNode, JsonNode.class);
                restTemplate.patchForObject(new URI(uri), updateVariantAirbag, JsonNode.class);
                JsonNode newVariantAirbag = restTemplate.getForObject(new URI(uri), JsonNode.class);
                return newVariantAirbag;
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public JsonNode newVariantAirbag(final JsonNode newVariant) {
        final String variantId = newVariant.get("variantId").asText();
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_AIRBAG).toString();
        final String uri_Variant = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).append("/").append(variantId).toString();
        try {
            JsonNode variant = restTemplate.getForObject(new URI(uri_Variant), JsonNode.class);
            if (!variant.isNull()) {
                if (!Objects.isNull(newVariant)) {
                    ObjectNode variantNode = (ObjectNode) newVariant;
                    JsonNode node = restTemplate.postForObject(new URI(uri), variantNode, JsonNode.class);
                    return node;
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void deleteVariantAirbag(final String id) throws ExceptionDeleteProject {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_AIRBAG).append("/").append(id).toString();
        try {
            restTemplate.delete(new URI(uri));
        } catch (URISyntaxException | HttpServerErrorException ex) {
            ex.printStackTrace();
            throw new ExceptionDeleteProject("Error when delete project");
        }
    }
}
