package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Service
public class ComponentDependService extends GenericProjectService<JsonNode> {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    private final String URI_COMPONENT_DEPEND = "project/componentDepend";
    private final String URI_COMPONENT_MAIN = "project/componentMain";

    @Override
    protected String getUrl() {
        return URI_COMPONENT_DEPEND;
    }


    public JsonNode updateComponentDepends(JsonNode componentDepends) {
        try {
            if (!componentDepends.isEmpty()) {
                if (componentDepends.hasNonNull(ConstantString.COMPONENT_ID) && componentDepends.get(ConstantString.COMPONENT_ID).asText() != null) {
                    String componentId = componentDepends.get(ConstantString.COMPONENT_ID).asText();
                    final String uriComponentMain = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_MAIN).append("/").append(componentId).toString();
                    JsonNode componentMain = restTemplate.getForObject(new URI(uriComponentMain), JsonNode.class);
                    if (componentMain != null) {
                        ObjectMapper mapper = new ObjectMapper();
                        ArrayNode component = mapper.createArrayNode();
                        JsonNode componentDepend = componentDepends.get("componentDepends");
                        if (componentDepend == null) {
                            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                        }
                        componentDepend.forEach(cd -> {
                            if (cd != null) {
                                if (cd.hasNonNull(ConstantString.ID) && cd.get(ConstantString.ID).asText() != null) {
                                    final String componentDependId = cd.get(ConstantString.ID).asText();
                                    JsonNode newComponentDepend = updateByField(cd, componentDependId);
                                    component.add(newComponentDepend);
                                }
                                if (!cd.hasNonNull(ConstantString.ID)) {
                                    JsonNode newComponentDepend = createComponentDepend(cd, componentId);
                                    component.add(newComponentDepend);
                                }
                            }
                        });
                        return component;
                    } else {
                        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
                    }
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }

            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private JsonNode createComponentDepend(final JsonNode componentDepend, final String componentMainId) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_DEPEND).toString();
        final KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            ObjectNode componentDependNode = (ObjectNode) componentDepend;
            if (componentDependNode == null) {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
            if (componentDependNode.get(ConstantString.ABHAENGIGKEIT).asText().equals(componentMainId)) {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
            if (!Objects.isNull(keycloakSecurityContext)) {
                componentDependNode.put(ConstantString.CREATED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                componentDependNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
            componentDependNode.put(ConstantString.COMPONENT_ID, componentMainId);
            componentDependNode.put(ConstantString.KONSTRUKTIV, jsonNodeModificationHelper.checkBool(componentDependNode, ConstantString.KONSTRUKTIV));
            componentDependNode.put(ConstantString.STRATEGISCH, jsonNodeModificationHelper.checkBool(componentDependNode, ConstantString.STRATEGISCH));
            componentDependNode.put(ConstantString.ABHAENGIGKEIT, checkAbhaengigkeit(componentDependNode, componentMainId));
            JsonNode newComp = restTemplate.postForObject(new URI(uri), componentDependNode, JsonNode.class);
            return newComp;

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        if (createNode.get(ConstantString.COMPONENT_ID) != null && createNode.hasNonNull(ConstantString.COMPONENT_ID)) {
            String componentId = createNode.get(ConstantString.COMPONENT_ID).asText();
            final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_MAIN).append("/").append(componentId).toString();
            try {
                JsonNode component = restTemplate.getForObject(new URI(uri), JsonNode.class);
                if (component != null) {
                    if (createNode.get(ConstantString.ABHAENGIGKEIT).asText().equals(componentId)) {
                        throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                    }
                    createNode.put(ConstantString.KONSTRUKTIV, jsonNodeModificationHelper.checkBool(createNode, ConstantString.KONSTRUKTIV));
                    createNode.put(ConstantString.STRATEGISCH, jsonNodeModificationHelper.checkBool(createNode, ConstantString.STRATEGISCH));
                    createNode.put(ConstantString.COMPONENT_ID, componentId);
                    createNode.put(ConstantString.ABHAENGIGKEIT, checkAbhaengigkeit(createNode, componentId));
                    return createNode;
                } else throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }

        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }

    }

    private String checkAbhaengigkeit(JsonNode node, String componentId) {
        try {
            if (node.hasNonNull(ConstantString.ABHAENGIGKEIT) && node.get(ConstantString.ABHAENGIGKEIT).asText() != null) {
                final String abhengigkeit = node.get(ConstantString.ABHAENGIGKEIT).asText();
                // Check exist component by abhengigkeit
                final String uriComponentMain = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_MAIN).append("/").append(abhengigkeit).toString();
                JsonNode component = restTemplate.getForObject(new URI(uriComponentMain), JsonNode.class);
                if (component == null) {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }

                // Check component have already abhengigkeit
                final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_DEPEND).append("/check/").append(componentId).append("/").append(abhengigkeit).toString();
                Boolean check = restTemplate.getForObject(new URI(uri), Boolean.class);
                if (check != null) {
                    if (check == Boolean.TRUE) {
                        throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                    } else {
                        return abhengigkeit;
                    }
                } else throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

}
