package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class DocumentService extends GenericService<JsonNode> {

    private final String URI_DOCUMENT = "glossary/document";

    @Override
    protected String getUrl() {
        return URI_DOCUMENT;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }

}
