package consulting.sit.catenax.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

@Service
@Slf4j
public class ImageService {

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;
    @Autowired
    private RestTemplate restTemplate;

    private String PREFIX_IMAGE = "image_";

    public String uploadImage(MultipartFile image) {
        log.info("Call to D2050 to upload image");
        String uploadUrl = d2050ServiceBaseurl + "images/upload";
        // Create HttpHeaders with appropriate headers, if needed
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        // Create a MultiValueMap to hold the request body
        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();

        Resource multipartFile = image.getResource();
        requestBody.add("file", multipartFile);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(requestBody, headers);
        ResponseEntity<String> uploadedFile = restTemplate.postForEntity(uploadUrl, requestEntity, String.class);
        return uploadedFile.getBody();
    }

    public Resource downloadImage(String relativeUrl) throws IOException {
        log.info("Call to D2050 to download image");
        String downloadUrl = d2050ServiceBaseurl + "images/download";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(downloadUrl)
                .queryParam("url", relativeUrl);

        final Resource image = restTemplate.getForObject(builder.build().toUri(), Resource.class);
        // Return the image file as a response with appropriate headers
        return image;
    }
}
