package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class NotificationService extends GenericService<JsonNode> {

    private final String URI_NOTIFICATION = "glossary/notification";

    @Override
    protected String getUrl() {
        return URI_NOTIFICATION;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
