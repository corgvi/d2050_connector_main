package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
public class ComponentDescriptionService extends GenericProjectService<JsonNode> {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    private final String URI_COMPONENT_DESCRIPTION = "project/componentDescription";
    private final String URI_COMPONENT_MAIN = "project/componentMain";


    @Override
    protected String getUrl() {
        return URI_COMPONENT_DESCRIPTION;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        if (createNode.get(ConstantString.COMPONENT_ID) != null && createNode.hasNonNull(ConstantString.COMPONENT_ID)) {
            String componentId = createNode.get(ConstantString.COMPONENT_ID).asText();
            final String uriComponentDescription = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_DESCRIPTION).append("/componentMain/").append(componentId).toString();
            final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_COMPONENT_MAIN).append("/").append(componentId).toString();
            try {
                JsonNode componentDescriptionNode = restTemplate.getForObject(new URI(uriComponentDescription), JsonNode.class);
                if (componentDescriptionNode == null) {
                    JsonNode component = restTemplate.getForObject(new URI(uri), JsonNode.class);
                    if (component != null) {
                        createNode.put(ConstantString.COMPONENT_ID, componentId);
                        createNode.put("beschichtet", jsonNodeModificationHelper.checkBool(createNode, "beschichtet"));
                        createNode.put("lackiert", jsonNodeModificationHelper.checkBool(createNode, "lackiert"));
                        createNode.put("elektronik", jsonNodeModificationHelper.checkBool(createNode, "elektronik"));
                        return createNode;
                    } else throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
                } else {
                    throw new HttpClientErrorException(HttpStatus.ALREADY_REPORTED);
                }
                } catch(URISyntaxException e){
                    throw new RuntimeException(e);
                }
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

}
