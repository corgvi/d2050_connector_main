package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class RegionService extends GenericService<JsonNode> {

    private final String URI_REGION = "glossary/region";

    @Override
    protected String getUrl() {
        return URI_REGION;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
