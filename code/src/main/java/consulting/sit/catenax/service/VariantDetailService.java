package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Service
public class VariantDetailService {
    private static final Logger LOGGER = LogManager.getLogger(VariantDetailService.class);
    private static final String URI_VARIANT_DETAIL = "project/variantDetail";
    private static final String URI_VARIANT = "project/variant";
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;
    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    public JsonNode getVariantDetailById(int id, String language) {
        final String uri = d2050ServiceBaseurl + "project/variantDetail/" + id;
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resultNode = mapper.createObjectNode();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            return node;
        } catch (URISyntaxException | HttpClientErrorException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public JsonNode updateVariantDetailByField(final JsonNode updateVariantDetail, final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_DETAIL).append("/").append(id).toString();
        final String uriVariant = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).append("/").append(id).toString();
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode oldVariantDetail = restTemplate.getForObject(new URI(uri), JsonNode.class);
            JsonNode variant = restTemplate.getForObject(new URI(uriVariant), JsonNode.class);
            ObjectNode variantNode = (ObjectNode) variant;
            if (!oldVariantDetail.isNull()) {
                if (!Objects.isNull(keycloakSecurityContext)) {
                    if (variantNode != null) {
                        int version = variant.get(ConstantString.VERSION).asInt();
                        version++;
                        variantNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                        variantNode.put(ConstantString.VERSION, version);
                    }
                }
                restTemplate.patchForObject(new URI(uriVariant), variantNode, JsonNode.class);
                restTemplate.patchForObject(new URI(uri), updateVariantDetail, JsonNode.class);
                JsonNode newVariant = restTemplate.getForObject(new URI(uri), JsonNode.class);
                return newVariant;
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Variant Detail not found in database");
            }

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public JsonNode updateVariantDetail(final JsonNode updateVariantDetail, final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_DETAIL).append("/").append(id).toString();
        final String uriVariant = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).append("/").append(id).toString();
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode oldVariantDetail = restTemplate.getForObject(new URI(uri), JsonNode.class);
            JsonNode variant = restTemplate.getForObject(new URI(uriVariant), JsonNode.class);;
            ObjectNode variantNode = (ObjectNode) variant;
            if (!oldVariantDetail.isNull()) {
                if (!Objects.isNull(keycloakSecurityContext)) {
                    if (variantNode != null) {
                        int version = variant.get(ConstantString.VERSION).asInt();
                        version++;
                        variantNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                        variantNode.put(ConstantString.VERSION, version);
                    }
                }
                restTemplate.patchForObject(new URI(uriVariant), variantNode, JsonNode.class);
                restTemplate.put(new URI(uri), updateVariantDetail);
                JsonNode newVariant = restTemplate.getForObject(new URI(uri), JsonNode.class);
                return newVariant;
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Variant Detail not found in database");
            }

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void deleteVariantDetail(final String id) throws ExceptionDeleteProject {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_DETAIL).append("/").append(id).toString();
        try {
            restTemplate.delete(new URI(uri));
        } catch (URISyntaxException | HttpServerErrorException ex) {
            ex.printStackTrace();
            throw new ExceptionDeleteProject("Error when delete project");
        }
    }

    public JsonNode newVariantDetail(final JsonNode newVariant) {
        final String variantId = newVariant.get("id").asText();
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_DETAIL).toString();
        final String uri_variant_detail = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_DETAIL).append("/").append(variantId).toString();
        final String uri_Variant = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).append("/").append(variantId).toString();
        try {
                JsonNode variant = restTemplate.getForObject(new URI(uri_Variant), JsonNode.class);
                if (!variant.isNull()) {
                    if (variant.get(ConstantString.VARIANT_DETAIL_ENTITY).isNull()){
                        if (!Objects.isNull(newVariant)) {
                            ObjectNode variantNode = (ObjectNode) newVariant;
                            JsonNode node = restTemplate.postForObject(new URI(uri), variantNode, JsonNode.class);
                            return node;
                        } else {
                            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Payload is null");
                        }
                    } else {
                        throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Variant Detail is exist in database");
                    }

                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Variant Detail not found in database");
                }

        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
}
