package consulting.sit.catenax.service.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.service.glossary.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
public class FixingDropLanguageMapService extends GenericLanguageMapService<JsonNode> {

    @Autowired
    private RestTemplate restTemplate;
    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;
    private final String URI_FIXING_DROP_LANGUAGE_MAPS = "glossary/fixingDropLanguageMap";
    private final String URI_FIXING_DROP_CODE_LANGUAGE_MAPS = "glossary/fixingDropCodeLanguageMap";

    @Override
    protected String getUrl() {
        return URI_FIXING_DROP_LANGUAGE_MAPS;
    }

    @Override
    public JsonNode parseResult(JsonNode glossaryObject) {
        if (glossaryObject != null) {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode returnNode = mapper.createArrayNode();
            glossaryObject.forEach(currentNode -> {
                ObjectNode objectNode = mapper.createObjectNode();
                objectNode.set(ConstantString.ID, currentNode.get(ConstantString.FIXING_DROP_LANGUAGE_MAP_PK).get(ConstantString.ID));
                String idOfFixingDrop = currentNode.get(ConstantString.FIXING_DROP_LANGUAGE_MAP_PK).get(ConstantString.ID).asText();
                String language = currentNode.get(ConstantString.FIXING_DROP_LANGUAGE_MAP_PK).get(ConstantString.LANGUAGE).asText();
                if (idOfFixingDrop != null && language != null) {
                    final String uriFixingDropCodeLanguageMap = new StringBuilder().append(d2050ServiceBaseurl).append(URI_FIXING_DROP_CODE_LANGUAGE_MAPS).append("/fixingDrop/").append(idOfFixingDrop).append("/").append(language).toString();
                    try {
                        JsonNode fixingDropCodeLanguageMap = restTemplate.getForObject(new URI(uriFixingDropCodeLanguageMap), JsonNode.class);
                        if (fixingDropCodeLanguageMap != null) {
                            if (fixingDropCodeLanguageMap.get(ConstantString.VALUE) != null) {
                                objectNode.set(ConstantString.VALUE, fixingDropCodeLanguageMap.get(ConstantString.VALUE));
                            } else {
                                objectNode.set(ConstantString.VALUE, null);
                            }
                        }
                    } catch (URISyntaxException e) {
                        throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                    }
                }
                objectNode.set(ConstantString.BESCHREIBUNG, currentNode.get(ConstantString.BESCHREIBUNG));
                objectNode.set(ConstantString.LANGUAGE, currentNode.get(ConstantString.FIXING_DROP_LANGUAGE_MAP_PK).get(ConstantString.LANGUAGE));
                returnNode.add(objectNode);
            });
            return returnNode;
        } else {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    protected JsonNode createObject(final ObjectNode createNode) {
        return null;
    }

}
