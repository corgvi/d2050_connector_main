package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class UiService extends GenericService<JsonNode> {
    private final String URI_UI = "glossary/ui";

    @Override
    protected String getUrl() {
        return URI_UI;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
