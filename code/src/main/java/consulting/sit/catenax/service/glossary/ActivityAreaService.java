package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class ActivityAreaService extends GenericService<JsonNode> {

    private final String URI_ACTIVITY_AREA_LANGUAGE_MAPS = "glossary/activityArea";

    @Override
    protected String getUrl() {
        return URI_ACTIVITY_AREA_LANGUAGE_MAPS;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }

}
