package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class RgSeparabilityService extends GenericService<JsonNode> {

    private final String URI_RG_SEPARABILITY = "glossary/rgSeparability";

    @Override
    protected String getUrl() {
        return URI_RG_SEPARABILITY;
    }


    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
