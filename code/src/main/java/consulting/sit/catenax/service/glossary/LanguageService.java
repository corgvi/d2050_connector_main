package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class LanguageService extends GenericService<JsonNode> {

    private final String URI_LANGUAGE = "glossary/language";

    @Override
    protected String getUrl() {
        return URI_LANGUAGE;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
