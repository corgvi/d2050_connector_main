package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Service
public class VariantService {
    private static final Logger LOGGER = LogManager.getLogger(VariantService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;
    private static final String URI_VARIANT =  "project/variant";
    private static final String URI_PROJECT = "project/project";
    private static final String URI_VARIANT_DETAIL = "project/variantDetail";

    public JsonNode getPartDetails(int id, String language) {
        final String uri = d2050ServiceBaseurl + "project/componentMain/variant/" + id;
        try {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode returnNode = mapper.createArrayNode();

            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (node != null) {
                node.forEach(currentSourceNode -> {
                    ObjectNode objectNode = mapper.createObjectNode();
                    objectNode.set(ConstantString.ID, currentSourceNode.get(ConstantString.ID));
                    if (currentSourceNode.get("componentReferenceEntity") != null) {
                        objectNode.set("nummer", currentSourceNode.get("componentReferenceEntity").get("teileNummer"));
                    } else {
                        objectNode.set("nummer", null);
                    }
                    objectNode.set("prefix", currentSourceNode.get("prefix"));
                    objectNode.set("suffix", currentSourceNode.get("suffix"));
                    objectNode.set("cpsc", currentSourceNode.get("cpsc"));
                    if (currentSourceNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY) != null
                            && currentSourceNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS) != null
                            && currentSourceNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language) != null
                    ) {
                        ObjectNode descriptionObject = mapper.createObjectNode();
                        descriptionObject.set(ConstantString.ID, currentSourceNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language).get("partLanguageMapPk").get(ConstantString.ID));
                        descriptionObject.set(ConstantString.LANGUAGE, currentSourceNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language).get("partLanguageMapPk").get(ConstantString.LANGUAGE));
                        descriptionObject.set(ConstantString.VALUE, currentSourceNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE));
                        objectNode.set(ConstantString.DESCRIPTION, descriptionObject);
                    } else {
                        objectNode.set(ConstantString.DESCRIPTION, null);
                    }
                    if (currentSourceNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY) != null
                            && currentSourceNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS) != null
                            && currentSourceNode.get(ConstantString.COMPONENT_MAIN_BENENNUNG_ENTITY).get(ConstantString.PART_LANGUAGE_MAPS).get(language) != null
                    ) {
                        ObjectNode descriptionObject = mapper.createObjectNode();
                        descriptionObject.set(ConstantString.ID, currentSourceNode.get(ConstantString.MATERIAL_MAIN_ENTITY).get(ConstantString.MATERIAL_MAIN_LANGUAGE_MAPS).get(language).get("materialMainLanguageMapPk").get(ConstantString.ID));
                        descriptionObject.set(ConstantString.LANGUAGE, currentSourceNode.get(ConstantString.MATERIAL_MAIN_ENTITY).get(ConstantString.MATERIAL_MAIN_LANGUAGE_MAPS).get(language).get("materialMainLanguageMapPk").get(ConstantString.LANGUAGE));
                        descriptionObject.set(ConstantString.VALUE, currentSourceNode.get(ConstantString.MATERIAL_MAIN_ENTITY).get(ConstantString.MATERIAL_MAIN_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE));
                        objectNode.set("material", descriptionObject);
                    } else {
                        objectNode.set("material", null);
                    }
                    objectNode.set(ConstantString.LEVEL_IN_BOM, currentSourceNode.get(ConstantString.LEVEL_IN_BOM));

                    returnNode.add(objectNode);
                });
            }
            return returnNode;
        } catch (URISyntaxException | HttpClientErrorException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public JsonNode getVariants(int id) {
        final String uri = d2050ServiceBaseurl + "project/componentMain/variant/" + id;
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode resultList = mapper.createArrayNode();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (node != null) {
                node.forEach(e -> {
                    ObjectNode resultNode = mapper.createObjectNode();
                    if (e.has(ConstantString.ID)) {
                        resultNode.set(ConstantString.ID, e.get(ConstantString.ID));
                    }

                    if (e.has("variantDetailEntity") && e.get("variantDetailEntity").has("fahrzeugbezeichnung")) {
                        resultNode.set("name", e.get("variantDetailEntity").get("fahrzeugbezeichnung"));
                    } else {
                        resultNode.put("name", "NO NAME");
                    }
                    resultList.add(resultNode);
                });
            }
            return resultList;
        } catch (URISyntaxException | HttpClientErrorException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public JsonNode getVariantDetails(int id, String language) {
        final String uri = d2050ServiceBaseurl + "project/variant/" + id;
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resultNode = mapper.createObjectNode();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (node != null) {
                resultNode.put(ConstantString.ID, node.get(ConstantString.ID).asInt());
                resultNode.put(ConstantString.DESCRIPTION, node.get(ConstantString.DESCRIPTION).asText());
                resultNode.put(ConstantString.NAME, getName(node, language));
                resultNode.put(ConstantString.MODIFIED_BY, node.get(ConstantString.MODIFIED_BY).asText());
                resultNode.put(ConstantString.MODIFIED_AT, node.get(ConstantString.MODIFIED_AT));
                resultNode.put(ConstantString.DISMANTLINGSTUDY, node.get(ConstantString.DISMANTLINGSTUDY).asBoolean());
                resultNode.put(ConstantString.UNTERSUCHUNGSTART, node.get(ConstantString.UNTERSUCHUNGSTART).asText());
                resultNode.put(ConstantString.UNTERSUCHUNGENDE, node.get(ConstantString.UNTERSUCHUNGENDE).asText());
                resultNode.put(ConstantString.VARIANT_DETAIL_ENTITY, node.get(ConstantString.VARIANT_DETAIL_ENTITY));
                resultNode.put(ConstantString.VARIANT_AIRBAG, node.get(ConstantString.VARIANT_AIRBAG));
                resultNode.put(ConstantString.CREATED_AT, node.get(ConstantString.CREATED_AT));
                resultNode.put(ConstantString.CREATED_BY, node.get(ConstantString.CREATED_BY).asText());
                resultNode.put(ConstantString.PROJECT_ID, node.get(ConstantString.PROJECT_ID).asText());
            }
            return resultNode;
        } catch (URISyntaxException | HttpClientErrorException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public JsonNode updateVariant(final JsonNode updateVariant, final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).append("/").append(id).toString();
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode oldVariant = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (!oldVariant.isNull()) {
                ObjectNode varianNode = (ObjectNode) updateVariant;
                if (!Objects.isNull(keycloakSecurityContext)) {
                    varianNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                }
                restTemplate.put(new URI(uri), varianNode);
                JsonNode newVariant = restTemplate.getForObject(new URI(uri), JsonNode.class);
                return newVariant;
            } else {
                return null;
            }

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public JsonNode updateVariantByField(final JsonNode updateVariant, final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).append("/").append(id).toString();
        final String uriVariantDetail = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_DETAIL).append("/").append(id).toString();
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode oldVariantUpdate = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (!oldVariantUpdate.isNull()) {
                ObjectNode variantNode = (ObjectNode) updateVariant;
                String name = null;

                if (variantNode.has(ConstantString.NAME)) {
                    name = updateVariant.get(ConstantString.NAME).asText();
//                    variantNode.remove(ConstantString.NAME);
                }

                if (!Objects.isNull(keycloakSecurityContext)) {
                    variantNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                }

                try {
                    restTemplate.patchForObject(new URI(uri), variantNode, JsonNode.class);
                } catch (HttpClientErrorException e){
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }

                ObjectMapper objectMapper = new ObjectMapper();
                ObjectNode variantDetailNode = objectMapper.createObjectNode();
                if (StringUtils.isNotEmpty(name)) {
                    variantDetailNode.put(ConstantString.FAHRZEUG_BEZEICHNUNG, name);
                }

                try {
                    restTemplate.patchForObject(new URI(uriVariantDetail), variantDetailNode, JsonNode.class);
                } catch (HttpClientErrorException e){
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }

                JsonNode newVariantUpdate = getVariantDetails(Integer.parseInt(id), "");
                return newVariantUpdate;
            } else {
                return null;
            }

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    /**
     * Returns name of variant.
     * Check variantDetailEntity, if it is not existing return "NO NAME".
     *
     * @param variant  JsonNode to look for the variantName
     * @param language Language you want to return the variantName
     * @return variantName from variantDetailEntity in given language or if not found "NO NAME"
     */
    protected String getName(JsonNode variant, String language) {
        if (variant != null
                && variant.get(ConstantString.VARIANT_DETAIL_ENTITY) != null
                && variant.get(ConstantString.VARIANT_DETAIL_ENTITY).get(ConstantString.FAHRZEUG_BEZEICHNUNG) != null) {
            return variant.get(ConstantString.VARIANT_DETAIL_ENTITY).get(ConstantString.FAHRZEUG_BEZEICHNUNG).asText();
        } else if (variant != null && variant.get(ConstantString.NAME) != null) {
            return variant.get(ConstantString.NAME).asText();
        } else {
            return "NO NAME";
        }
    }

    public JsonNode newVariant(final JsonNode newVariant) {
        final String projectId = newVariant.get("projectId").asText();
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).toString();
        final String uri_variant_detail = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT_DETAIL).toString();
        final String uri_Project = new StringBuilder().append(d2050ServiceBaseurl).append(URI_PROJECT).append("/").append(projectId).toString();
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode project = restTemplate.getForObject(new URI(uri_Project), JsonNode.class);
            if (!project.isNull()) {
                if (!Objects.isNull(newVariant)) {
                    ObjectNode variantNode = (ObjectNode) newVariant;
                    if (!Objects.isNull(keycloakSecurityContext)) {
                        String name = null;
                        variantNode.put(ConstantString.CREATED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                        variantNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                        if (variantNode.has(ConstantString.NAME)) {
                            name = newVariant.get(ConstantString.NAME).asText();
//                            variantNode.remove(ConstantString.NAME);
                        }
                        JsonNode node = restTemplate.postForObject(new URI(uri), variantNode, JsonNode.class);
                        String id = node.get(ConstantString.ID).asText();
                        ObjectMapper objectMapper = new ObjectMapper();
                        ObjectNode variantDetailNode = objectMapper.createObjectNode();
                        variantDetailNode.put(ConstantString.ID, id);
                        if (StringUtils.isNotEmpty(name)) {
                            variantDetailNode.put(ConstantString.FAHRZEUG_BEZEICHNUNG, name);
                        }
                        variantDetailNode.put(ConstantString.BATTERIETYPHV, false);
                        variantDetailNode.put(ConstantString.BATTERIETYP12V, false);
                        variantDetailNode.put(ConstantString.BATTERIETYP24V, false);
                        variantDetailNode.put(ConstantString.BATTERIETYP48V, false);
                        try {
                            restTemplate.postForObject(new URI(uri_variant_detail) ,variantDetailNode , JsonNode.class);
                        } catch (HttpClientErrorException ex) {
                            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                        }

                        JsonNode variant = getVariantDetails(Integer.parseInt(id), "");
                        return variant;
                    } else {
                        throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
                    }
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void deleteVariant(final String id) throws ExceptionDeleteProject {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_VARIANT).append("/").append(id).toString();
        try {
            restTemplate.delete(new URI(uri));
        } catch (URISyntaxException | HttpServerErrorException ex) {
            ex.printStackTrace();
            throw new ExceptionDeleteProject("Error when delete project");
        }
    }
}
