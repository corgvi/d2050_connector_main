package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class MarkIdentService extends GenericService<JsonNode> {
    private final String URI_MARK_IDENT = "glossary/markIdent";

    @Override
    protected String getUrl() {
        return URI_MARK_IDENT;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
