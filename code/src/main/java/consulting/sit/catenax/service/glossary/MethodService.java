package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class MethodService extends GenericService<JsonNode> {

    private final String URI_METHOD = "glossary/method";

    @Override
    protected String getUrl() {
        return URI_METHOD;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
