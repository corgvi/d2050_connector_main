package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class FixingDropCodeService extends GenericService<JsonNode> {

    private final String URI_FIXING_DROP_CODE = "glossary/fixingDropCode";

    @Override
    protected String getUrl() {
        return URI_FIXING_DROP_CODE;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }

}
