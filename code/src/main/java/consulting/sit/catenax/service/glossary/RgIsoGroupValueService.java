package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class RgIsoGroupValueService extends GenericService<JsonNode> {

    private final String URI_RG_ISO_GROUP_VALUE = "glossary/rgIsoGroupValue";

    @Override
    protected String getUrl() {
        return URI_RG_ISO_GROUP_VALUE;
    }

    @Override
    protected JsonNode CreateObject(ObjectNode createNode) {
        return null;
    }
}
