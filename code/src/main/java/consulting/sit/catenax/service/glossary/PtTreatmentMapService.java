package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class PtTreatmentMapService extends GenericService<JsonNode> {

    private final String URI_PT_TREATMENT_MAP = "glossary/ptTreatmentMap";

    @Override
    protected String getUrl() {
        return URI_PT_TREATMENT_MAP;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
