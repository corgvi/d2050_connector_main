package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class StatusService {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Value("${catenax.connector.baseurl}")
    private String catenaXConnectorBaseurl;

    public JsonNode getStatus(int anzahl) {
        String entries = "";
        if (anzahl > 0) {
            entries = "?entries=" + anzahl;
        }

        final String uri = catenaXConnectorBaseurl + "status/status" + entries;

        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            node.forEach(e -> {
                if (e instanceof ObjectNode) {
                    ObjectNode objectNode = (ObjectNode) e;

                    jsonNodeModificationHelper.removeAllBut(objectNode, List.of("id", "name"));
                }
            });
            return node;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JsonNode getVariants(int id) {
        final String uri = catenaXConnectorBaseurl + "project/variant/project/" + id;
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode resultList = mapper.createArrayNode();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);

            node.forEach(e -> {
                ObjectNode resultNode = mapper.createObjectNode();
                if (e.has("id")) {
                    resultNode.set("id", e.get("id"));
                }

                if (e.has("variantDetailEntity") && e.get("variantDetailEntity").has("fahrzeugbezeichnung")) {
                    resultNode.set("name", e.get("variantDetailEntity").get("fahrzeugbezeichnung"));
                } else {
                    resultNode.put("name", "NO NAME");
                }
                resultList.add(resultNode);
            });
            return resultList;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
}
