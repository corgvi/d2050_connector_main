package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class MaterialGroupService extends GenericService<JsonNode> {

    private final String URI_MATERIAL_GROUP = "glossary/materialGroup";

    @Override
    protected String getUrl() {
        return URI_MATERIAL_GROUP;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
