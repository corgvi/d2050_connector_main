package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class FixingMainService extends GenericService<JsonNode> {
    private final String URI_FIXING_MAIN = "glossary/fixingMain";

    @Override
    protected String getUrl() {
        return URI_FIXING_MAIN;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
