package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class PtMaterialMapService extends GenericService<JsonNode> {

    private final String URI_PT_MATERIAL_MAP = "glossary/ptMaterialMap";

    @Override
    protected String getUrl() {
        return URI_PT_MATERIAL_MAP;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
