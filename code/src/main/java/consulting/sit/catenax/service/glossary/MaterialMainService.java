package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class MaterialMainService extends GenericService<JsonNode> {
    private final String URI_MATERIAL_MAIN = "glossary/materialMain";

    @Override
    protected String getUrl() {
        return URI_MATERIAL_MAIN;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
