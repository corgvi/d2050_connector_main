package consulting.sit.catenax.service.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.service.glossary.GenericService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

@Service
public class ActivityAreaLanguageMapService extends GenericLanguageMapService<JsonNode> {

    private final String URI_ACTIVITY_AREA_LANGUAGE_MAPS = "glossary/activityAreaLanguageMap";

    @Override
    protected String getUrl() {
        return URI_ACTIVITY_AREA_LANGUAGE_MAPS;
    }

    @Override
    public JsonNode parseResult(JsonNode glossaryObject) {
        if (glossaryObject != null) {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode returnNode = mapper.createArrayNode();
            glossaryObject.forEach(currentNode -> {
                ObjectNode objectNode = mapper.createObjectNode();
                objectNode.set(ConstantString.ID, currentNode.get(ConstantString.ACTIVITY_AREA_LANGUAGE_MAP_PK).get(ConstantString.ID));
                if (currentNode.get(ConstantString.VALUE) != null) {
                    objectNode.set(ConstantString.VALUE, currentNode.get(ConstantString.VALUE));
                } else {
                    objectNode.set(ConstantString.VALUE, null);
                }
                objectNode.set(ConstantString.LANGUAGE, currentNode.get(ConstantString.ACTIVITY_AREA_LANGUAGE_MAP_PK).get(ConstantString.LANGUAGE));
                returnNode.add(objectNode);
            });
            return returnNode;
        } else {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    protected JsonNode createObject(final ObjectNode createNode) {
        return null;
    }

}
