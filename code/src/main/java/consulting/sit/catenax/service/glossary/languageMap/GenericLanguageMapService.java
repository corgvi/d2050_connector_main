package consulting.sit.catenax.service.glossary.languageMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Service
public abstract class GenericLanguageMapService<T> {

    @Autowired
    private RestTemplate restTemplate;
    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;
    protected abstract String getUrl();

    public T getAllByLanguage(String language) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).append("/").append(language).toString();
        try {
            JsonNode glossaryObject = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (glossaryObject != null) {
                return parseResult(glossaryObject);
            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (URISyntaxException e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    protected abstract T parseResult(JsonNode glossaryObject);

    public void deleteLanguage(final String id, final String language) throws ExceptionDeleteProject {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).append("/").append(id).append("/").append(language).toString();
        try {
            restTemplate.delete(new URI(uri));
        } catch (URISyntaxException | HttpServerErrorException ex) {
            ex.printStackTrace();
            throw new ExceptionDeleteProject("Error when delete project");
        }
    }

    public T create(T newObject) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).toString();
        try {
            if (!Objects.isNull(newObject)) {
                ObjectNode node = (ObjectNode) newObject;
                    createObject(node);
                JsonNode newNode = restTemplate.postForObject(new URI(uri), node, JsonNode.class);
                return (T) newNode;
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
    protected abstract JsonNode createObject(ObjectNode createNode);

    public T findById(final String id, final String language) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).append("/").append(id).append("/").append(language).toString();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (node == null) {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
            return (T) node;
        } catch (URISyntaxException | HttpServerErrorException ex) {
            ex.printStackTrace();
            throw new ExceptionDeleteProject("Error when delete project");
        }
    }

    public T update(final JsonNode node, final String id, final String language) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).append("/").append(id).append("/").append(language).toString();
        try {
            if (node != null) {
                return (T) restTemplate.patchForObject(new URI(uri), node, JsonNode.class);
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
        } catch (URISyntaxException | HttpServerErrorException ex) {
            ex.printStackTrace();
            throw new ExceptionDeleteProject("Error when delete project");
        }
    }
}
