package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.service.KeycloakAuthenticationService;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Service
public abstract class GenericService<T> {

    @Autowired
    private RestTemplate restTemplate;
    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;
    protected abstract String getUrl();

    public void delete(final String id) throws ExceptionDeleteProject {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).append("/").append(id).toString();
        try {
            restTemplate.delete(new URI(uri));
        } catch (URISyntaxException | HttpServerErrorException ex) {
            ex.printStackTrace();
            throw new ExceptionDeleteProject("Error when delete project");
        }
    }

    public T create(T newObject) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).toString();
        try {
            if (!Objects.isNull(newObject)) {
                ObjectNode node = (ObjectNode) newObject;
                CreateObject(node);
                JsonNode newNode = restTemplate.postForObject(new URI(uri), node, JsonNode.class);
                return (T) newNode;
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
    protected abstract JsonNode CreateObject(ObjectNode createNode);

    public T getAll() {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).toString();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (node != null) {
                return (T) node;
            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (URISyntaxException e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    public T findById(final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(getUrl()).append("/").append(id).toString();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (node != null) {
                return (T) node;
            } else {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
        } catch (URISyntaxException e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }
}
