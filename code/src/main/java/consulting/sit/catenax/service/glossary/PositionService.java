package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class PositionService extends GenericService<JsonNode> {

    private final String URI_POSITION = "glossary/position";

    @Override
    protected String getUrl() {
        return URI_POSITION;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
