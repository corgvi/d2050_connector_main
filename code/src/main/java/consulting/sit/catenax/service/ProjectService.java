package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.exceptions.ExceptionDeleteProject;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

@Service
public class ProjectService {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Autowired
    private KeycloakAuthenticationService keycloakAuthenticationService;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    private static final String URI_PROJECT =  "project/project/";

    public JsonNode getProjects() {
        final String uri = d2050ServiceBaseurl + "project/project";
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            node.forEach(e -> {
                if (e instanceof ObjectNode) {
                    ObjectNode objectNode = (ObjectNode) e;

                    jsonNodeModificationHelper.removeAllBut(objectNode, List.of(ConstantString.ID, ConstantString.NAME, ConstantString.CREATED_BY, ConstantString.CREATED_AT, ConstantString.DESCRIPTION));
                }
            });
            return node;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public JsonNode newProject(JsonNode newProject) {
    	final String uri = d2050ServiceBaseurl + "project/project";
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            if (!Objects.isNull(newProject)) {
                ObjectNode projectNode = (ObjectNode) newProject;
                if (!Objects.isNull(keycloakSecurityContext)) {
                    projectNode.put(ConstantString.CREATED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                    projectNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                    JsonNode node = restTemplate.postForObject(new URI(uri), projectNode, JsonNode.class);
                    return node;
                } else {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
                }
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JsonNode getProjectDetails(int id, String language) {
        final String uri = d2050ServiceBaseurl + "project/project/" + id;
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resultNode = mapper.createObjectNode();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (node != null) {
                resultNode.put(ConstantString.ID, node.get(ConstantString.ID).asInt());
                resultNode.put(ConstantString.DESCRIPTION, node.get(ConstantString.DESCRIPTION).asText());
                resultNode.put(ConstantString.NAME, node.get(ConstantString.NAME).asText());
                resultNode.put(ConstantString.CREATED_BY, node.get(ConstantString.CREATED_BY).asText());
                resultNode.put(ConstantString.CREATED_AT, node.get(ConstantString.CREATED_AT));
                resultNode.put(ConstantString.MODIFIED_BY, node.get(ConstantString.MODIFIED_BY).asText());
                resultNode.put(ConstantString.MODIFIED_AT, node.get(ConstantString.MODIFIED_AT));
                resultNode.put(ConstantString.CXPROJECT, node.get(ConstantString.CXPROJECT).asBoolean());
            }
            return resultNode;
        } catch (URISyntaxException | HttpClientErrorException e) {
        	e.printStackTrace();
            return null;
        }
    }
    
    protected String getName(JsonNode variant, String language) {
        if (variant != null
                && variant.get(ConstantString.VARIANT_DETAIL_ENTITY) != null
                && variant.get(ConstantString.VARIANT_DETAIL_ENTITY).get(ConstantString.FAHRZEUG_BEZEICHNUNG) != null) {
            return variant.get(ConstantString.VARIANT_DETAIL_ENTITY).get(ConstantString.FAHRZEUG_BEZEICHNUNG).asText();
        } else {
            return "NO NAME";
        }
    }
    
    public JsonNode getVariants(int id) {
        final String uri = d2050ServiceBaseurl + "project/variant/project/" + id;
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode resultList = mapper.createArrayNode();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);

            node.forEach(e -> {
                ObjectNode resultNode = mapper.createObjectNode();

                // id
                resultNode.set(ConstantString.ID, e.get(ConstantString.ID));

                // project id
                resultNode.set(ConstantString.PROJECT_ID, e.get(ConstantString.PROJECT_ID));

                // name
                if (e.has(ConstantString.VARIANT_DETAIL_ENTITY) && e.get(ConstantString.VARIANT_DETAIL_ENTITY).has(ConstantString.FAHRZEUG_BEZEICHNUNG)) {
                    resultNode.set(ConstantString.NAME, e.get(ConstantString.VARIANT_DETAIL_ENTITY).get(ConstantString.FAHRZEUG_BEZEICHNUNG));
                } else {
                    resultNode.put(ConstantString.NAME, "NO NAME");
                }

                // created at
                resultNode.set(ConstantString.CREATED_AT, e.get(ConstantString.CREATED_AT));

                // created by
                resultNode.set(ConstantString.CREATED_BY, e.get(ConstantString.CREATED_BY));

                // description
                resultNode.set(ConstantString.DESCRIPTION, e.get(ConstantString.DESCRIPTION));

                //modifierBy
                resultNode.set(ConstantString.MODIFIED_BY, e.get(ConstantString.MODIFIED_BY));

                //modifierAt
                resultNode.set(ConstantString.MODIFIED_AT, e.get(ConstantString.MODIFIED_AT));

                //dismantlingStudy
                resultNode.set(ConstantString.DISMANTLINGSTUDY, e.get(ConstantString.DISMANTLINGSTUDY));

                //untersuchungStart
                resultNode.set(ConstantString.UNTERSUCHUNGSTART, e.get(ConstantString.UNTERSUCHUNGSTART));

                //untersuchungEnde
                resultNode.set(ConstantString.UNTERSUCHUNGENDE, e.get(ConstantString.UNTERSUCHUNGENDE));


                resultList.add(resultNode);
            });
            return resultList;
        } catch (URISyntaxException | HttpClientErrorException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JsonNode updateProject(final JsonNode updateProject, final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_PROJECT).append(id).toString();
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode oldProjectUpdate = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (!oldProjectUpdate.isNull()) {
                ObjectNode projectNode = (ObjectNode) updateProject;
                if (!Objects.isNull(keycloakSecurityContext)) {
                    projectNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                }
                restTemplate.put(new URI(uri), projectNode);
                JsonNode newProjectUpdate = restTemplate.getForObject(new URI(uri), JsonNode.class);
                return newProjectUpdate;
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public JsonNode updateProjectByField(final JsonNode updateProject, final String id) {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_PROJECT).append(id).toString();
        KeycloakSecurityContext keycloakSecurityContext = keycloakAuthenticationService.getKeycloakSecurityContext();
        try {
            JsonNode oldProjectUpdate = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (!oldProjectUpdate.isNull()) {
                ObjectNode projectNode = (ObjectNode) updateProject;
                if (!Objects.isNull(keycloakSecurityContext)) {
                    projectNode.put(ConstantString.MODIFIED_BY, keycloakSecurityContext.getToken().getPreferredUsername());
                }
                restTemplate.patchForObject(new URI(uri), projectNode, JsonNode.class);
                JsonNode newProjectUpdate = restTemplate.getForObject(new URI(uri), JsonNode.class);
                return newProjectUpdate;
            } else {
                throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
            }

        } catch (URISyntaxException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public void deleteProject(final String id) throws ExceptionDeleteProject {
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_PROJECT).append(id).toString();
        try {
            restTemplate.delete(new URI(uri));
        } catch (URISyntaxException | HttpServerErrorException ex) {
            ex.printStackTrace();
            throw new ExceptionDeleteProject("Error when delete project");
        }
    }
}
