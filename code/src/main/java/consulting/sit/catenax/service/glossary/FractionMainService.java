package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class FractionMainService extends GenericService<JsonNode> {

    private final String URI_FRACTION_MAIN = "glossary/fractionMain";

    @Override
    protected String getUrl() {
        return URI_FRACTION_MAIN;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        if (createNode.hasNonNull("active")) {
            if (createNode.get("active") != null) {
                createNode.put("active", createNode.get("active"));
            } else {
                createNode.put("active", "false");
            }
        }
        return createNode;
    }
}
