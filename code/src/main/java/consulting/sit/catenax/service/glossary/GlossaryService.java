package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
public class GlossaryService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    private final String URI_GLOSSARY = "glossary/table";

    public JsonNode getAllTableName(){
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_GLOSSARY).toString();
        try {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode returnNode = mapper.createArrayNode();
            JsonNode glossaryTableName = restTemplate.getForObject(new URI(uri), JsonNode.class);
            glossaryTableName.forEach(e -> {
                ObjectNode node = mapper.createObjectNode();
                node.set(ConstantString.NAME, e.get(ConstantString.NAME));
                returnNode.add(node);
            });
            return returnNode;
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public JsonNode getDateModelByTableName(String tableName){
        final String uri = new StringBuilder().append(d2050ServiceBaseurl).append(URI_GLOSSARY).append("/").append(tableName).toString();
        try {
            JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
            if (node == null) {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            }
            return node;
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
