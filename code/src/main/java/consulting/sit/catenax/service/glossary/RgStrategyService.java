package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class RgStrategyService extends GenericService<JsonNode> {

    private final String URI_RG_STRATEGY = "glossary/rgStrategy";

    @Override
    protected String getUrl() {
        return URI_RG_STRATEGY;
    }


    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
