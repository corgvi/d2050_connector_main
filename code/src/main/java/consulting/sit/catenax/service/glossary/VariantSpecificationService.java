package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class VariantSpecificationService extends GenericService<JsonNode> {
    private final String URI_VARIANT_SPECIFICATION = "glossary/variantSpecification";

    @Override
    protected String getUrl() {
        return URI_VARIANT_SPECIFICATION;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
