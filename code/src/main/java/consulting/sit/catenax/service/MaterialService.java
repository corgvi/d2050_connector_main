package consulting.sit.catenax.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.constants.ConstantString;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
public class MaterialService {
    private static final Logger LOGGER = LogManager.getLogger(MaterialService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JsonNodeModificationHelper jsonNodeModificationHelper;

    @Value("${d2050.service.baseurl}")
    private String d2050ServiceBaseurl;

    public JsonNode getMaterialDetail(int materialId, String language) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resultNode = mapper.createObjectNode();
        final String urimaterial = d2050ServiceBaseurl + "glossary/materialMain/" + materialId;

        try {
            JsonNode materialNode = restTemplate.getForObject(new URI(urimaterial), JsonNode.class);

            resultNode.put("id", getId(materialNode));
            resultNode.put("name", getName(materialNode, language));
            // VDA and ISO
            if (materialNode.has(ConstantString.MATERIAL_GROUP_ENTITY)
                    && materialNode.get(ConstantString.MATERIAL_GROUP_ENTITY) != null) {
                // VDA
                ObjectNode vdaNode = mapper.createObjectNode();
                vdaNode.set("hg", materialNode.get(ConstantString.MATERIAL_GROUP_ENTITY).get("vdaHg"));
                vdaNode.set("ug", materialNode.get(ConstantString.MATERIAL_GROUP_ENTITY).get("vdaUg"));
                resultNode.set("vda_group", vdaNode);

                // ISO
                ObjectNode isoNode = mapper.createObjectNode();
                isoNode.set("standard", materialNode.get(ConstantString.MATERIAL_GROUP_ENTITY).get("standard"));
                isoNode.set("nummer", materialNode.get(ConstantString.MATERIAL_GROUP_ENTITY).get("standardNummer"));
                resultNode.set("iso_group", isoNode);
            }

            return resultNode;
        } catch (URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }


    /**
     * Returns name of material.
     * Check materialMainLanguageMaps, if it is not existing return "NO NAME".
     *
     * @param material JsonNode to look for the materialName
     * @param language Language you want to return the materialName
     * @return materialName from materialMainLanguageMaps in given language or if not found "NO NAME"
     */
    private String getName(JsonNode material, String language) {
        if (material != null
                && material.get(ConstantString.MATERIAL_MAIN_LANGUAGE_MAPS) != null
                && material.get(ConstantString.MATERIAL_MAIN_LANGUAGE_MAPS).get(language) != null
                && material.get(ConstantString.MATERIAL_MAIN_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE) != null) {
            return material.get(ConstantString.MATERIAL_MAIN_LANGUAGE_MAPS).get(language).get(ConstantString.VALUE).asText();
        } else {
            return "NO NAME";
        }
    }

    /**
     * Returns id of material.
     * Check if materialId exists and return it.
     *
     * @param material JsonNode to look for the id
     * @return id from the given material, null if materialId is not found
     */
    private Long getId(JsonNode material) {
        if (material != null
                && material.get(ConstantString.ID) != null) {
            return material.get(ConstantString.ID).asLong();
        }
        return null;
    }
}
