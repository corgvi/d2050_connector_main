package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class FractionWasteService extends GenericService<JsonNode> {

    private final String URI_FRACTION_WASTE = "glossary/fractionWaste";

    @Override
    protected String getUrl() {
        return URI_FRACTION_WASTE;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
