package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class MaterialValueService extends GenericService<JsonNode> {

    private final String URI_MATERIAL_VALUE = "glossary/materialValue";

    @Override
    protected String getUrl() {
        return URI_MATERIAL_VALUE;
    }


    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
