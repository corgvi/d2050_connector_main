package consulting.sit.catenax.service.glossary;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class CompoundService extends GenericService<JsonNode> {

    private final String URI_COMPOUND_LANGUAGE_MAPS = "glossary/compound";

    @Override
    protected String getUrl() {
        return URI_COMPOUND_LANGUAGE_MAPS;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }


}
