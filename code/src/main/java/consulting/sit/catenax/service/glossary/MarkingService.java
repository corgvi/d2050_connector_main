package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class MarkingService extends GenericService<JsonNode> {

    private final String URI_MARKING = "glossary/marking";

    @Override
    protected String getUrl() {
        return URI_MARKING;
    }

    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
