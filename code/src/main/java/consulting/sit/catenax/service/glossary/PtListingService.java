package consulting.sit.catenax.service.glossary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class PtListingService extends GenericService<JsonNode> {

    private final String URI_PTLISTING = "glossary/ptListing";

    @Override
    protected String getUrl() {
        return URI_PTLISTING;
    }


    @Override
    protected JsonNode CreateObject(final ObjectNode createNode) {
        return null;
    }
}
