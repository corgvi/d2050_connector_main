package consulting.sit.catenax.helper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class JsonNodeModificationHelper {

    /**
     * Removes elements on level 1 from JsonNode that are not in the collection.
     *
     * @param objectNode          ObjectNode to remove elements from
     * @param elementsNotToRemove Collection with names of elements not to be removed
     */
    public void removeAllBut(ObjectNode objectNode, Collection<String> elementsNotToRemove) {
        List<String> removeList = new ArrayList<>();
        objectNode.fieldNames().forEachRemaining(e -> {
            if (!elementsNotToRemove.contains(e)) {
                removeList.add(e);
            }
        });
        objectNode.remove(removeList);
    }

    /**
     * Check boolean field in jsonNode is has not null
     *
     * @param node, fieldName
     * @param false
     */
    public String checkBool(JsonNode node, String fieldName){
        if (node.hasNonNull(fieldName) && node.get(fieldName).asText() != null) {
            return node.get(fieldName).asText();
        } else {
            return Boolean.FALSE.toString();
        }
    }
}
