//package consulting.sit.catenax.security;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
//import org.springframework.security.web.SecurityFilterChain;
//
//@Configuration
//public class WebSecurityConfig {
//
//    @Bean
//    public KeycloakJwtAuthenticationConverter  jwtAuthenticationConverter() {
//        return new KeycloakJwtAuthenticationConverter();
//    }
//    @Bean
//    public SecurityFilterChain httpSecurityFilterChain(HttpSecurity http) throws Exception {
//        http
//                .csrf().disable()
//                .authorizeRequests()
////                .anyRequest().permitAll()
////                .mvcMatchers("/images/**").permitAll()
////                .mvcMatchers("/js/**").permitAll()
////                .mvcMatchers("/admin/**").hasAnyRole("Admin", "ReserveAdmin")
////                .mvcMatchers("/project/**").hasAnyAuthority("Admin", "Editor", "Salesperson")
//                .anyRequest().authenticated()
//                .and()
//                .oauth2ResourceServer(oauth2 -> oauth2.jwt(jwt -> jwt.jwtAuthenticationConverter(jwtAuthenticationConverter())))
//        ;
//
////        http
////                .csrf().disable()
////                .authorizeRequests()
//////                .anyRequest().permitAll()
//////                .mvcMatchers("/images/**").permitAll()
//////                .mvcMatchers("/js/**").permitAll()
//////                .mvcMatchers("/admin/**").hasAnyRole("Admin", "ReserveAdmin")
//////                .mvcMatchers("/project/**").hasAnyAuthority("Admin", "Editor", "Salesperson")
////                .anyRequest().authenticated()
////        ;
//        return http.build();
//    }
//}
//
//
//
////package consulting.sit.catenax.security;
////
////import org.springframework.security.config.annotation.web.builders.HttpSecurity;
////import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
////import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
////
////@EnableWebSecurity
////public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
////
////    @Override
////    protected void configure(HttpSecurity http) throws Exception {
////        http
////                .cors().and()
////                .csrf().disable()
////                .authorizeRequests()
////                .anyRequest().permitAll();
//////                .mvcMatchers("/images/**").permitAll()
//////                .mvcMatchers("/js/**").permitAll()
//////                .mvcMatchers(HttpMethod.GET, "/glossary/**").permitAll()
//////                .mvcMatchers("/glossary/**").hasAnyRole("Admin", "Glossaryverwalter")
//////                .mvcMatchers("/project/**").hasAnyAuthority("Admin", "Editor", "Salesperson")
//////                .anyRequest().authenticated()
//////                 .and()
//////                 .rememberMe().key("AbcdEfghIjklmNopQrsTuvXyz_0123456789");
////        http.headers().frameOptions().sameOrigin();
////    }
////}