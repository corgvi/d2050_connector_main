# D2050 Connector

## Deployment Instructions

Steps to do before building the docker image:


### Create Trust Store

To create the trust store, you have to save the certificate from the website in the browser (e.g. from https://auth.tec4u.com).  
With the downloaded certificate you have to execute the following command:

```
"%JAVA_HOME%\bin\keytool" -import -v -trustcacerts -alias <alias name> -file <downloaded certificate> -keystore <root path of java program>\<truststore name> -storepass <truststore password> -noprompt
```

<ins>Variables</ins>:  
**alias name**: The alias name for the certificate.  
**downloaded certificate**: The name of the certificate downloaded from the website.  
**root path of java program**: The java source root path. This is where pom.xml is located, too.  
**truststore name**: The name of the truststore.
**truststore password**: The password for the truststore.


<ins>Example</ins>:  
```
"%JAVA_HOME%\bin\keytool" -import -v -trustcacerts -alias *.tec4u.com -file certificate.crt -keystore c:\workspace\d2050-connector\code\cacerts.jks -storepass changeit -noprompt
```